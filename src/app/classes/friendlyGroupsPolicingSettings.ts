export class FriendlyGroupsPolicingSettings {
    _id?: string;
    enabled: boolean;
    names: string;
    responseMsg: string;
}