export class Command {
    _id?: string;
    name: string;
    details: string;
}