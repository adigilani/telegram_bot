export class ScheduleMessage {
    _id?: string;
    interval: number;
    text: string;
}