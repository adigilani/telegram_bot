export class BannedWordsForUserNames {
    _id?: string;
    enabled: boolean;
    companyName: string;
    words: string;
}