export class BotSettings {
    _id?: string;
    welcomeMsg: string;
    banMsg: string;
    helpMsg: string;
    botMsgInDM: string;
    welcomeAggregatorNumber: number;
}