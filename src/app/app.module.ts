import { NgModule, Component, Input, ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { EmojiPickerModule } from 'ng-emoji-picker';

import { SimpleNotificationsModule } from 'angular2-notifications';
import { AlertModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { routing } from './app.routes';
import { LoginService } from './services/LoginController';
import { SpappComponent } from './components/spapp/spapp.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ChangePwdComponent } from './components/change-pwd/change-pwd.component';

@NgModule({
  declarations: [
    AppComponent,
    SpappComponent,
    LoginComponent,
    NavbarComponent,
    ChangePwdComponent
  ],
  imports: [
    BrowserModule,
    EmojiPickerModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    SimpleNotificationsModule.forRoot(),
    TabsModule.forRoot(),
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    FormsModule,
    RouterModule,
    routing
  ],
  entryComponents: [ ChangePwdComponent ],
  providers: [LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
