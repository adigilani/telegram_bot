import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { SpappComponent } from './components/spapp/spapp.component';


// Route Configuration
export const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'spapp', component: SpappComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);