import { Injectable } from '@angular/core';
import { User } from '../classes/user';

import axios from 'axios';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Injectable()
export class LoginService {
  private user = new User();
  
  // private serverURL = 'https://tbot-demo.herokuapp.com';
  // private authURL = this.serverURL.concat('/auth');

  private authURL = '/auth';

  constructor(
    private _router: Router, 
  ) {

  }

  public isUserLoggedIn() {
    return this.user.isUserLoggedIn;
  }

  public isUserAuthorized() {
    if(!this.user.isUserLoggedIn) {
      this._router.navigate([``]);
    }
  }

  public currentLoggedInUser() {
    return this.user;
  }

  public logout(){
    this.user.isUserLoggedIn = false;
    this._router.navigate([``]);
  }

  public changePWD(pwd) {
    const userObj = new User();
    userObj._id = this.user._id;
    userObj.username = this.user.username;
    userObj.password = pwd;
    return new Promise((resolve, reject) => {
      axios.post(`${this.authURL}`, userObj)
      .then((response)=>{
        this.user._id = response.data._id;
        this.user.password = userObj.password;
        resolve(true);
      }).catch(()=>{
        return reject(false);
      });
    });
  }

  public doLogin(loginData: User) {
    return new Promise((resolve, reject) => {
      axios.get(`${this.authURL}/${loginData.username}`)
      .then((response)=>{
        if(response.data) {
          if(response.data.password==loginData.password) {
            this.user._id = response.data._id;
            this.user.username = loginData.username;
            this.user.password = loginData.password;
            this.user.isUserLoggedIn = true;
            resolve(true);
          } else {
            return resolve(false);
          }
        } else {
          return resolve(false);
        }
      }).catch(()=>{
        return reject(false);
      });
    });
  }

}