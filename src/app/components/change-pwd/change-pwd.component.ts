import axios from 'axios';
import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { LoginService } from '../../services/LoginController';
import { User } from '../../classes/user';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'change-pwd',
  templateUrl: './change-pwd.component.html',
  styleUrls: ['./change-pwd.component.css']
})

export class ChangePwdComponent implements OnInit {
  
  formPWDObj = {
    current: "",
    newPWD: "",
    newPWD1: ""
  }

  errText = "";

  constructor(
    private _LoginService: LoginService,
    public bsModalRef: BsModalRef,
    private _notificationService: NotificationsService
  ) {}
 
  ngOnInit() {
  }

  savePassword() {
    this.errText = "";
    if(this.formPWDObj.current!=this._LoginService.currentLoggedInUser().password) {
      this.errText = "Please provide your previous password";
      return;
    }

    if(this.formPWDObj.newPWD != this.formPWDObj.newPWD1) {
      this.errText = "Provided passwords doesn't match";
      return;
    }

    this._LoginService.changePWD(this.formPWDObj.newPWD).then((response)=>{
      this.bsModalRef.hide();
      this._notificationService.success("success", "Password Changed");
    }).catch((err)=>{
      this.bsModalRef.hide();
      this._notificationService.error("error", "Couldn't Change Password");
    });
  }
}