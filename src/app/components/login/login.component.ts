import * as _ from 'lodash';
import { Component, OnInit } from '@angular/core';
import { User } from '../../classes/user';
import { NotificationsService } from 'angular2-notifications';
import { LoginService } from '../../services/LoginController';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userObj = new User();

  constructor(
    private _notificationService: NotificationsService,
    private _router: Router,
    private _LoginService: LoginService
  ) { }

  ngOnInit() {
  }

  doLogin() {
    if(_.isEmpty(this.userObj.username) || _.isEmpty(this.userObj.password)) {
      this._notificationService.error("Error", "Username and password cannot be empty");
      return;
    }

    this._LoginService.doLogin(this.userObj)
    .then((response)=>{
      if(!response) {
        this._notificationService.error("Error", "Login failed.");
      } else {
        this._router.navigate(['spapp']);
      }
    }).catch((err)=>{
      this._notificationService.error("Error", "Login failed.");
    });
  }
}
