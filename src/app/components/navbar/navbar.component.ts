import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/LoginController';
import { BsModalService } from 'ngx-bootstrap';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ChangePwdComponent } from '../change-pwd/change-pwd.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(
    private _LoginService: LoginService,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
  }

  isLoggedIn() {
    return this._LoginService.isUserLoggedIn();
  }

  logout() {
    this._LoginService.logout();
  }

  changePWD(){
    this.bsModalRef = this.modalService.show(ChangePwdComponent);
    this.bsModalRef.content.closeBtnName = 'Close'; 
  }

}
