import * as _ from 'lodash';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import axios from 'axios';
import { NotificationsService } from 'angular2-notifications';
import { TabsetComponent } from 'ngx-bootstrap';
import * as moment from 'moment';

import { Command } from '../../classes/command';
import { ScheduleMessage } from '../../classes/schedule_message';
import { BotSettings } from '../../classes/botSettings';
import { FriendlyGroupsPolicingSettings } from '../../classes/friendlyGroupsPolicingSettings';
import { BannedWordsForUserNames } from '../../classes/bannedWordsForUserNames';

import { debuglog } from 'util';

import { LoginService } from '../../services/LoginController';

@Component({
  selector: 'spapp',
  templateUrl: './spapp.component.html',
  styleUrls: ['./spapp.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SpappComponent implements OnInit {
  @ViewChild('scheduledMsgsTabs') scheduledMsgsTabs: TabsetComponent;


  // private serverURL = 'https://tbot-demo.herokuapp.com';
  // private commandsURL = this.serverURL.concat('/commands');
  // private scheduledMsgsURL = this.serverURL.concat('/scheduled');
  // private settingsURL = this.serverURL.concat('/settings');
  // private bannedWordsForUserNamePoliceURL = this.serverURL.concat('/police/bannedwords');
  // private friendlyGroupsPoliceURL = this.serverURL.concat('/police/friendlygroups');
  
  private commandsURL = '/commands';
  private scheduledMsgsURL = '/scheduled';
  private settingsURL = '/settings';
  private bannedWordsForUserNamePoliceURL = '/police/bannedwords';
  private friendlyGroupsPoliceURL = '/police/friendlygroups';

  constructor(
    private _service: NotificationsService,
    private _LoginService: LoginService
  ) { }

  ngOnInit() {
    this._LoginService.isUserAuthorized();

    this.getCommands();
    this.newCommand = new Command();
    
    this.getScheduledMsgs();
    this.newScheduledMsg = new ScheduleMessage();

    this.getSettings();
    this.getBannedWordsForUserNamesSettings();
    this.getFriendlyGroupsPolicingSettings();
  }

  ////////////////////////////// Friendly Group Policy /////////////////////////////////////////////////

  openPopupFriendlyGroupsPolicingSettings: Function;
  setPopupActionFriendlyGroupsPolicingSettings(fn: any) {
    this.openPopupFriendlyGroupsPolicingSettings = fn;
  }

  friendlyGroupsPolicingSettings = new FriendlyGroupsPolicingSettings();

  getFriendlyGroupsPolicingSettings() {
    axios.get(this.friendlyGroupsPoliceURL)
    .then((settings)=>{
      if(settings.data && settings.data.length>0) {
        settings.data.forEach((setting)=> {
          this.friendlyGroupsPolicingSettings._id = setting._id;
          this.friendlyGroupsPolicingSettings.enabled = setting.enabled;
          this.friendlyGroupsPolicingSettings.names = setting.names;
          this.friendlyGroupsPolicingSettings.responseMsg = setting.responseMsg;
        });
      } else {
        console.log('Nothing retrieved');
      }
    }).catch((err)=>{
      this._service.error("Error", "Failed to retrieve friendly groups settings");
      console.log('Nothing retrieved. Error: ' + err);
    });
  }

  saveFriendlyGroupsPolicingSettings() {
    axios.post(this.friendlyGroupsPoliceURL, this.friendlyGroupsPolicingSettings)
    .then((response)=>{
      if(response.data) {
        this.friendlyGroupsPolicingSettings._id = response.data._id;
        this._service.success("Success", "Settings saved!");
      }
    }).catch((err)=>{
        console.log(err);
        this._service.error("Error", "Settings wasn't saved");
    });
  }

  //////////////////////////////  Banned Words For User Names /////////////////////////////////////////////////

  openPopupBannedWordsForUserNames: Function;
  setPopupActionBannedWordsForUserNames(fn: any) {
    this.openPopupBannedWordsForUserNames = fn;
  }

  bannedWordForUserNamesSettings = new BannedWordsForUserNames();

  getBannedWordsForUserNamesSettings() {
    axios.get(this.bannedWordsForUserNamePoliceURL)
    .then((settings)=>{
      if(settings.data && settings.data.length>0) {
        settings.data.forEach((setting)=> {
          this.bannedWordForUserNamesSettings._id = setting._id;
          this.bannedWordForUserNamesSettings.enabled = setting.enabled;
          this.bannedWordForUserNamesSettings.companyName = setting.companyName;
          this.bannedWordForUserNamesSettings.words = setting.words;
        });
      } else {
        console.log('Nothing retrieved');
      }
    }).catch((err)=>{
      this._service.error("Error", "Failed to retrieve banned words for user names settings");
      console.log('Nothing retrieved. Error: ' + err);
    });
  }

  bannedWordForUserNamesSettingsSaveButonEnabled() {
    return this.bannedWordForUserNamesSettings.companyName && this.bannedWordForUserNamesSettings.companyName.trim().length>0;
  }


  saveBannedWordsForUserNamesSettings() {
    axios.post(this.bannedWordsForUserNamePoliceURL, this.bannedWordForUserNamesSettings)
    .then((response)=>{
      if(response.data) {
        this.bannedWordForUserNamesSettings._id = response.data._id;
        this._service.success("Success", "Settings saved!");
      }
    }).catch((err)=>{
        console.log(err);
        this._service.error("Error", "Settings wasn't saved");
    });
  }

  ////////////////////////////// Commands /////////////////////////////////////////////////////

  commands: Command[] = [];
  selectedCommand: Command;
  newCommand: Command;

  openPopupAddCmd: Function;
  setPopupActionAddCmd(fn: any) {
    this.openPopupAddCmd = fn;
  }

  resetNewCommandForm() {
    this.newCommand.name = '';
    this.newCommand.details = '';
  }

  selectCommand(command: Command) {
    this.selectedCommand = command
  }

  getIndexOfCommand = (commandId: String) => {
    return this.commands.findIndex((command) => command._id === commandId);
  }

  getCommands() {
    axios.get(this.commandsURL)
    .then((commands) => {
      if(commands.data && commands.data.length>0) {
        commands.data.forEach((command)=> {
          this.commands.push(command);
        });
      } else {
        console.log('Nothing retrieved');
      }
    }).catch((err)=>{
      this._service.error("Error", "Failed to retrieve commands");
      console.log('Nothing retrieved. Error: ' + err);
    });
  }

  deleteCommand(commandId: String) {
    var idx = this.getIndexOfCommand(commandId);
    if (idx !== -1) {
      const commandToBeDeleted = this.commands[idx];
      axios.delete(`${this.commandsURL}/${commandToBeDeleted._id}`).then((response)=>{
        this.commands.splice(idx, 1);
        this.selectCommand(null);
        this._service.success("Success", "Command Deleted!");
      }).catch((err)=>{
        this._service.error("Error", "Failed to delete command");
        console.log("error: " + err);
      });
    }
  }

  addNewCommand():void {

    if(this.newCommand.name.length<=0 || this.newCommand.details.length<=0) {
      this._service.error("Error", "Empty contents");
      return;
    }
    
    this.newCommand.name = this.newCommand.name.toLowerCase();
    if(this.newCommand.name.charAt(0)=='!') {
      this.newCommand.name = this.newCommand.name.substring(1, this.newCommand.name.length);
    }

    axios.post(this.commandsURL, this.newCommand).then((response)=>{
      if(response.data) {
        this.newCommand._id = response.data._id;
        this.commands.push(_.cloneDeep(this.newCommand));
        this.selectCommand(this.newCommand);
        this.newCommand = new Command();
        this._service.success("Success", "Command Created!");
      }
    }).catch((err)=>{
        console.log(err);
        this._service.error("Error", "Command wasn't added");
    })
  }

  updateCommand(command: Command){
    var idx = this.getIndexOfCommand(command._id);
    if (idx !== -1) {
      this.commands[idx] = command;
      this.selectCommand(command);
    }
    return this.commands;
  }

  //////////////////////////////// Settings //////////////////////////////////////////////////////////////

  settings = new BotSettings();

  openPopupWelcomeMsg: Function;
  setPopupActionWelcomeMsg(fn: any) {
    this.openPopupWelcomeMsg = fn;
  }

  openPopupBotMsgInDM: Function;
  setPopupActionBotMsgInDM(fn: any) {
    this.openPopupBotMsgInDM = fn;
  }

  openPopupBanMsg: Function;
  setPopupActionBanMsg(fn: any) {
    this.openPopupBanMsg = fn;
  }

  openPopupHelpMsg: Function;
  setPopupActionHelpMsg(fn: any) {
    this.openPopupHelpMsg = fn;
  }

  saveButonEnabled() {
    return this.settings.botMsgInDM && this.settings.botMsgInDM.length>0 && this.settings.botMsgInDM.search('{commands}')>=0
  }

  setAggregatedUserValuesForWelcomeMessage(number) {
    this.settings.welcomeAggregatorNumber = number;
  }

  composeAggregatorDropdownMsg(number) {
    let aggregatorPhrase = "";
    if(number==0){
      aggregatorPhrase = "Bot is set to not greet any new user";
    } else if(number==1) {
      aggregatorPhrase = "Bot is set to greet every new user";
    } else {
      aggregatorPhrase = `Bot is set to greet ${number} new users together`;
    }
    return aggregatorPhrase;
  }

  saveSettings() {
    if(!this.settings.botMsgInDM || this.settings.botMsgInDM.trim().length<=0) {
      this._service.error("Error", "Cannot leave 'Bot response in DM' empty");
      return;
    }
    if(this.settings.botMsgInDM.search('{commands}')<=0) {
      this._service.error("Error", "You have to add {commands} in 'Bot response in DM'");
      return;
    }

    //this.settings.botMsgInDM = _.replace(this.settings.botMsgInDM, "{commands}", "\n\n{commands}");

    axios.post(this.settingsURL, this.settings).then(()=>{
      this._service.success("Success", "Settings have been saved");
      this.getSettings();
    }).catch((err)=>{
      console.log(err);
      this.settings.welcomeAggregatorNumber = 0;
      this._service.error("Error", "Settings wasn't changed");
    });
  }

  getSettings() {
    axios.get(this.settingsURL).then((setting)=>{
      if(setting.data.length>0) {
        this.settings.welcomeMsg = setting.data[0].welcomeMsg;
        this.settings.banMsg = setting.data[0].banMsg;
        this.settings.helpMsg = setting.data[0].helpMsg;
        this.settings.botMsgInDM = setting.data[0].botMsgInDM;
        // this.settings.botMsgInDM = _.replace(setting.data[0].botMsgInDM, "\n\n", "");
        this.settings.welcomeAggregatorNumber = setting.data[0].welcomeAggregatorNumber || 0;
      } else {
        console.log("No settings found.");
        this.settings.welcomeAggregatorNumber = 0;
      }
    }).catch((err)=>{
      console.log(err);
      this._service.error("Error", "Couldn't retrieve bot settings");
    });
  }

  ////////////////////////////// Scheduled Msgs /////////////////////////////////////////////////////

  openPopupNewScheduledMsg: Function;
  setPopupActionNewScheduledMsg(fn: any) {
    this.openPopupNewScheduledMsg = fn;
  }

  scheduledMsgs: ScheduleMessage[] = [];
  selectedScheduledMsg: ScheduleMessage;
  newScheduledMsg: ScheduleMessage;
  addNewMsgTabHeader = "Add new Message";

  selectScheduledMsg(scheduledMsg: ScheduleMessage) {
    this.selectedScheduledMsg = scheduledMsg
  }

  getIndexOfScheduledMsg = (scheduledMsgId: String) => {
    return this.scheduledMsgs.findIndex((scheduledMsg) => scheduledMsg._id === scheduledMsgId);
  }

  composeTimerDropdownMsg(interval) {
    let timerPhrase = "";
    if(interval>0){
      var diff = moment.duration(interval);
      if(diff.asHours()<1) {
        timerPhrase = `Timer set as once every ${diff.asMinutes()} minutes`;
      } else {
        timerPhrase = `Timer set as once every ${diff.asHours()} hours`;
      }
    } else {
      timerPhrase = "Timer is turned off";
    }
    return timerPhrase;
  }

  setSchedulerTimer(interval) {
    this.newScheduledMsg.interval = interval * 60 * 1000;
  }

  getScheduledMsgs() {
    axios.get(this.scheduledMsgsURL)
    .then((scheduledMsgs) => {
      if(scheduledMsgs.data && scheduledMsgs.data.length>0) {
        scheduledMsgs.data.forEach((command)=> {
          this.scheduledMsgs.push(command);
        });
      } else {
        console.log('Nothing retrieved');
      }
    }).catch((err)=>{
      this._service.error("Error", "Failed to retrieve schedule messages");
      console.log('Nothing retrieved. Error: ' + err);
    });
  }

  resetNewScheduledMsgForm() {
    this.newScheduledMsg.interval = 0;
    this.newScheduledMsg = new ScheduleMessage();
    this.addNewMsgTabHeader = "Add new Message";
  }

  editScheduledMsg(scheduledMsgId: String) {
    var idx = this.getIndexOfScheduledMsg(scheduledMsgId);
    if (idx !== -1) {
      this.addNewMsgTabHeader = "Edit Message";
      this.scheduledMsgsTabs.tabs[1].active = true;
      this.newScheduledMsg = _.cloneDeep(this.scheduledMsgs[idx]);
    }
  }

  deleteScheduledMsg(scheduledMsgId: String) {
    var idx = this.getIndexOfScheduledMsg(scheduledMsgId);
    if (idx !== -1) {
      const scheduledMsgToBeDeleted = this.scheduledMsgs[idx];
      axios.delete(`${this.scheduledMsgsURL}/${scheduledMsgToBeDeleted._id}`).then((response)=>{
        this.scheduledMsgs.splice(idx, 1);
        this.selectScheduledMsg(null);
        this._service.success("Success", "Message Deleted!");
      }).catch((err)=>{
        this._service.error("Error", "Failed to delete command");
        console.log("error: " + err);
      });
    }
  }

  saveScheduledMsgButonEnabled() {
    return this.newScheduledMsg.text && this.newScheduledMsg.text.length>0
  }


  addNewScheduledMsg():void {
    if(this.newScheduledMsg.text.length<=0 ) {
      this._service.error("Error", "Empty message cannot be saved");
      return;
    }

    const oldRecId = this.newScheduledMsg._id;
    
    axios.post(this.scheduledMsgsURL, this.newScheduledMsg).then((response)=>{
      if(response.data) {
        if(oldRecId) {
          var idx = this.getIndexOfScheduledMsg(oldRecId);
          if (idx !== -1) {
            this.scheduledMsgs.splice(idx, 1);
          }      
        }
        this.addNewMsgTabHeader = "Add new Message";
        this.newScheduledMsg._id = response.data._id;
        this.scheduledMsgs.push(_.cloneDeep(this.newScheduledMsg));
        this.selectScheduledMsg(this.newScheduledMsg);
        this.newScheduledMsg = new ScheduleMessage();
        this._service.success("Success", "Message Saved!");
        this.scheduledMsgsTabs.tabs[0].active = true;
      }
    }).catch((err)=>{
        console.log(err);
        this._service.error("Error", "Message wasn't saved");
    })
  }
}