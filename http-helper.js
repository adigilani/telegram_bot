'use strict';

const axios = require('axios');

class HTTP_HELPER {

    constructor(serverURL) {
        'use strict';
        console.log("Initializing HTTP_HELPER");
        
        this.SERVER_URL = serverURL;

        this.USERS_URL = `${this.SERVER_URL}users`;
        this.COMMANDS_URL = `${this.SERVER_URL}commands`;
        this.SCHEDULED_MSGS_URL = `${this.SERVER_URL}scheduled`;
        this.SETTINGS_URL = `${this.SERVER_URL}settings`;
        this.POLICING_BANNED_WORDS_SETTINGS_URL = `${this.SERVER_URL}police/bannedwords`;
        this.POLICING_FRIENDLY_GROUPS_SETTINGS_URL = `${this.SERVER_URL}police/friendlygroups`;
    }

    reportUsers(reportedUsers){
        'use strict';
        const requests = [];
        reportedUsers.forEach(reportedUser => {
            requests.add(
                axios.post(this.USERS_URL, reportedUser)
            );
        });
        return axios.all(requests);
    }

    getReportedUser(username){
        'use strict';
        return axios.get(`${this.USERS_URL}`);
    }

    getAllReportedUsers() {
        'use strict';
        return axios.get(`${this.USERS_URL}`);
    }

    removeReportedUser(usersToBeDeleted){
        'use strict';
        const requests = [];
        usersToBeDeleted.forEach(userToBeDeleted => {
            requests.add(
                axios.delete(`${this.USERS_URL}/user/${userToBeDeleted}`)
            );
        });
        return axios.all(requests);
    }

    updateUsers(updatedUsers){
        'use strict';
        const requests = [];
        updatedUsers.forEach(updatedUser => {
            requests.add(
                axios.put(`${this.USERS_URL}/doc/${updatedUser._id}`, updatedUser)
            );
        });
        return axios.all(requests);
    }

    getAllCommands(){
        'use strict';
        return axios.get(`${this.COMMANDS_URL}`);
    }

    getCommand(command){
        'use strict';
        return axios.get(`${this.COMMANDS_URL}/${command}`);
    }

    getScheduledMsgs(){
        'use strict';
        return axios.get(`${this.SCHEDULED_MSGS_URL}`);
    }

    removeScheduledMsg(scheduledMsgId){
        'use strict';
        return axios.delete(`${this.SCHEDULED_MSGS_URL}/${scheduledMsgId}`)
    }

    addScheduledMsg(scheduledMsg){
        'use strict';
        return axios.post(this.SCHEDULED_MSGS_URL, scheduledMsg)
    }

    getSettings(){
        'use strict';
        return axios.get(`${this.SETTINGS_URL}`);
    }

    getBannedWordsDictionary(){
        'use strict';
        return axios.get(`${this.POLICING_BANNED_WORDS_SETTINGS_URL}`);
    }

    getFriendlyGroupsSettings(){
        'use strict';
        return axios.get(`${this.POLICING_FRIENDLY_GROUPS_SETTINGS_URL}`);
    }
}

module.exports = HTTP_HELPER;