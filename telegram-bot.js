'use strict';

const _ = require('lodash');
const linkify = require('linkifyjs');
const Bot = require('node-telegram-bot-api');
const format = require("string-template");
const logger = require('winston');
const https = require("https");
const FastMap = require("collections/fast-map");
const FastSet = require("collections/fast-set");
const List = require("collections/list");
const HTTP_HELPER = require('./http-helper');


// Configure logger settings
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, { colorize: true});
logger.level = 'info';

// BOT Settings
const BOT_SETTINGS = require('./telegram-bot-settings.json');

logger.info('Telegram Bot Server Starting...');
const BOT_FRIENDS = process.env.BOT_FRIENDS || BOT_SETTINGS.BOT_FRIENDS;
const BOT_TOKEN = process.env.BOT_TOKEN || BOT_SETTINGS.TOKEN;
const SERVER_URL = process.env.SERVER_URL || BOT_SETTINGS.SERVER_URL;
const BOT_USERNAME = process.env.BOT_USERNAME || BOT_SETTINGS.BOT_USERNAME;
const MONGODB_URI = process.env.MONGODB_URI || BOT_SETTINGS.MONGODB_URI;
var MASTER_GROUP_CHAT_ID = process.env.MASTER_GROUP_CHAT_ID || BOT_SETTINGS.MASTER_GROUP_CHAT_ID;
const MASTER_GROUP_USERNAME = process.env.MASTER_GROUP_USERNAME || BOT_SETTINGS.MASTER_GROUP_USERNAME;
const PREDEFINED_MSGS = BOT_SETTINGS.PREDEFINED_MSGS;
const PREDEFINED_ADMINS =  BOT_SETTINGS.ADMINS;

logger.info(`Bot Username ${BOT_USERNAME}`);
logger.info(`Bot Friends ${BOT_FRIENDS}`);
logger.info(`Bot Auth Token ${BOT_TOKEN}`);
logger.info(`Server URL ${SERVER_URL}`);
logger.info(`MONGODB URL ${MONGODB_URI}`);
logger.info(`MASTER GROUP CHAT ID ${MASTER_GROUP_CHAT_ID}`);

// Initializing HTTP_HELPER
const APIHelper = new HTTP_HELPER(SERVER_URL);

var settingBootupComplete = false;
var policingBannedWordsRetrievalComplete = false;
var policingFriendlyGroupsRetrievalComplete = false;
var retrieveScheduledMsgsComplete = false;

var externalTelegramGroupsInviteScanEnabled = false;
var companyNameForBannedWordsForUserNamesScanning = [];
var bannedWordsForUserNamesScanEnabled = false;
const friendlyTelegramGroupsDictionary = new FastSet();
const bannedWordsForUserNamesDictionary = new FastSet();
const newUsersListForGreetingWithWelcomeMessages = new List();
var ADMINS =  BOT_SETTINGS.ADMINS;
const schedular = new FastMap();

// Initializing BOT Object
var bot;
bot = new Bot(BOT_TOKEN);
logger.info(`Setting webhook at ${SERVER_URL} ${bot.token}`);
bot.setWebHook(SERVER_URL + bot.token).then(()=>{
    logger.info("Webhook setup complete");
    getChannelAdmins();
    bot.unbanChatMember(MASTER_GROUP_CHAT_ID, 18838548).then(()=>{
        console.log("unbanned");
    }).catch((err)=>{
        console.log(`Couldn't unban you - ${err}`);
    });
});

logger.info('Telegram Bot Started...');

// bot webhook errors
bot.on('webhook_error', (error) => {
    logger.info(`webhook did not start. Error: ${error.code}`);
});

bot.on('message', function(msg) {

    if(!msg || msg.length<=0 || !msg.chat || !msg.chat.id) {
        return;
    }

    if(!MASTER_GROUP_CHAT_ID) {
        MASTER_GROUP_CHAT_ID = msg.chat.id;
    }

    if(msg.chat && msg.text) {
        treatAsChat(msg);
    } else if(msg.new_chat_members) {
        treatAsNewJoiner(msg);
    }
});

function treatAsChat(msg) {

    if(checkIfContainsGoupInvites(msg)) {
        return;
    }

    if(msg.from && isBannedWordUsedForUserNames(msg.from)){
        return;
    }

    // if it is a bot command then reply
    if(msg.text.charAt(0)==='/') {
        const userChatID = msg.from.id;
        const keyword = msg.text ? msg.text.toLowerCase().trim() : '';
        const userInDM = msg.chat.type == 'private' ;
    
        const treatedKeyword = treatKeyword(keyword)
        switch(treatedKeyword) {
            case 'start':
            case 'help':
                var greeting = "";
                try {
                    APIHelper.getAllCommands().then((commands) => {
                        if(commands.data && commands.data.length>0) {
                            var retrievedCommands = [];
                            commands.data.forEach((command)=> {
                                retrievedCommands.push(`/${command.name}\n`);
                            });
                            const retrievedCommandsFormatted = retrievedCommands.toString().replace(/,/g, "")
                            if(retrievedCommandsFormatted.length>0) {
                                greeting = format(PREDEFINED_MSGS.BOT_DM_HELP, {
                                    commands: retrievedCommandsFormatted
                                });
                                bot.sendMessage(userChatID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                    if(MASTER_GROUP_CHAT_ID && !userInDM) {
                                        if(PREDEFINED_MSGS.DM_INVITE_MESSAGE && PREDEFINED_MSGS.DM_INVITE_MESSAGE.length>0) {
                                            var greeting = '';
                                            if(PREDEFINED_MSGS.DM_INVITE_MESSAGE.search("{name}")>=0) {
                                                greeting = format(PREDEFINED_MSGS.DM_INVITE_MESSAGE, {
                                                    name: msg.from.first_name
                                                });
                                            } else {
                                                greeting = PREDEFINED_MSGS.DM_INVITE_MESSAGE;
                                            }
                                            bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                                logger.info(`sent message -> ${greeting}`);
                                            }).catch((err)=>{
                                                logger.info(`error -> ${err}`);
                                            });
                                        }
                                    }
                                }).catch((err)=>{
                                    logger.info(`User ${msg.from.first_name} never spoke to BOt: ${BOT_USERNAME} before. Send an invite`);
                                    const greeting = `Hey ${msg.from.first_name}, let's talk. click (t.me/${BOT_USERNAME})`;
                                    bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                        logger.info(`sent message -> ${greeting}`);
                                    }).catch((err)=>{
                                        logger.info(`error -> ${err}`);
                                    });
                                });
                            } else {
                                logger.info("No command exist");
                                greeting = PREDEFINED_MSGS.ERROR;
                                bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                    logger.info(`sent message -> ${greeting}`);
                                }).catch((err)=>{
                                    logger.info(`error -> ${err}`);
                                });
                            }
                        } else {
                            logger.info("api didn't retrieve docs");
                            greeting = PREDEFINED_MSGS.ERROR;
                            bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                logger.info(`sent message -> ${greeting}`);
                            }).catch((err)=>{
                                logger.info(`error -> ${err}`);
                            });;
                        }
                    }).catch((err)=>{
                        logger.info(err);
                        greeting = PREDEFINED_MSGS.ERROR;
                        bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                            logger.info(`sent message -> ${greeting}`);
                        }).catch((err)=>{
                            logger.info(`error -> ${err}`);
                        });
                    });
                } catch(err) {
                    logger.info(err);
                    greeting = PREDEFINED_MSGS.ERROR;
                    bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                        logger.info(`sent message -> ${greeting}`);
                    }).catch((err)=>{
                        logger.info(`error -> ${err}`);
                    });
                }
                break;
            default:
                if(userInDM) {
					var greeting = "";
                    APIHelper.getCommand(treatedKeyword).then((response) => {
                        if(response.data) {
                            bot.sendMessage(userChatID, response.data.details, {"parse_mode":"HTML"}).then(()=>{
                                logger.info(`sent message -> ${response.data.details}`);
                            }).catch((err)=>{
                                logger.info(`error -> ${err}`);
                            });
                        } else {
                            logger.info(`No such command retrieved, probably none exist - cmd: ${treatedKeyword}`);
                            greeting = PREDEFINED_MSGS.ERROR;
                            bot.sendMessage(userChatID, greeting, {"parse_mode":"HTML"}).then(()=>{
                                logger.info(`sent message -> ${greeting}`);
                            }).catch((err)=>{
                                logger.info(`error -> ${err}`);
                            });
                        }
                    }).catch((err)=>{
                        logger.info(err);
                        greeting = PREDEFINED_MSGS.ERROR;
                        bot.sendMessage(userChatID, greeting, {"parse_mode":"HTML"}).then(()=>{
                            logger.info(`sent message -> ${greeting}`);
                        }).catch((err)=>{
                            logger.info(`error -> ${err}`);
                        });
                    });
                } else {
                    var greeting = format(PREDEFINED_MSGS.UNKNOWN_COMMAND_REPLY, {
                        name: `${msg.from.first_name} [@${msg.from.username}]`
                    });
                    bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                        logger.info(`sent message -> ${greeting}`);
                    }).catch((err)=>{
                        logger.info(`error -> ${err}`);
                    });
                }
        }
    }
}

function isBannedWordUsedForUserNames(user) {
    logger.info("isBannedWordUsedForUserNames - entry");
    var issueFound = false;
    if(bannedWordsForUserNamesScanEnabled) {
        const dictionary = _.flattenDeep([Array.from(bannedWordsForUserNamesDictionary)]);
        const companyBrands = _.flattenDeep([companyNameForBannedWordsForUserNamesScanning]);
        const userNames = _.deburr(user.first_name.concat(' ').concat(user.last_name)).toLowerCase();
        const wordsInNames = _.words(userNames);

        if(       
            ((_.intersection(dictionary, wordsInNames).length>0) && (_.intersection(companyBrands, wordsInNames).length>0)) &&
            !_.flattenDeep([BOT_FRIENDS, BOT_USERNAME, ADMINS]).includes(user.username)
        ) {
            bot.kickChatMember(MASTER_GROUP_CHAT_ID, user.id).then((response)=>{
                logger.log(`kicked out a spammer ${response}`);
            }).catch((err)=>{
                logger.log(`Couldn't kick out a spammer ${err}`);
            });
            issueFound = true;
        }
    }
    logger.info("isBannedWordUsedForUserNames - exit");
    return issueFound;
}

function checkIfContainsGoupInvites(msg) {
    logger.info("checkIfContainsAuRL : Entry");

    const userInDM = msg.chat.type == 'private' ;
    var foundOffensiveLink = false;

    if(externalTelegramGroupsInviteScanEnabled==true) {
        if(!userInDM && !_.flattenDeep([BOT_FRIENDS, BOT_USERNAME, ADMINS]).includes(msg.from.username)) {
            var links = linkify.find(msg.text);
            var foundTelegramLink = false;

            const offendedLinkIsFriendly = _.find(links, (link)=>{
                if(link.value.search("t.me")>=0 || link.value.search("telegram.me")>=0) {
                    const splits = link.value.split('/');
                    const foundGroupName = _.last(splits);
                    foundTelegramLink = true;
                    return friendlyTelegramGroupsDictionary.has(foundGroupName.trim().toLowerCase());
                } else {
                    return false;
                }
            });

            if(!offendedLinkIsFriendly && foundTelegramLink) {
                bot.deleteMessage(msg.chat.id, msg.message_id).then(()=>{
                    var greeting = '';
                    if(PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE.search("{name}")>=0) {
                        greeting = format(PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE, {
                            name: `${msg.from.first_name} [@${msg.from.username}]`
                        });
                    } else {
                        greeting = PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE;
                    }
                    bot.sendMessage(MASTER_GROUP_CHAT_ID, greeting, {"parse_mode":"HTML"}).then(()=>{
                        logger.info(`sent message -> ${greeting}`);
                    }).catch((err)=>{
                        logger.info(`error -> ${err}`);
                    });
                    foundOffensiveLink = true;
                }).catch((err)=>{
                    logger.info("Was not able to remove the message -  error: " + err);
                });
            }
        }
    } else {
        logger.info("This feature is not enabled");
    }

    logger.info("checkIfContainsAuRL : Exit");
    return foundOffensiveLink;
}

function treatAsNewJoiner(msg) {
    var chatID = msg.chat.id;
    _.each(msg.new_chat_members, function(member) {
        if(!_.flattenDeep([BOT_FRIENDS, BOT_USERNAME, ADMINS]).includes(member.username)) {
            if(member.is_bot) {
                const result = bot.kickChatMember(chatID, member.id);
                if(PREDEFINED_MSGS.BOT_KICKED && PREDEFINED_MSGS.BOT_KICKED.length>0) {
                    var greeting = '';
                    if(PREDEFINED_MSGS.BOT_KICKED.search("{name}")>=0) {
                        greeting = format(PREDEFINED_MSGS.BOT_KICKED, {
                            name: member.first_name
                        });
                    } else {
                        greeting = PREDEFINED_MSGS.BOT_KICKED;
                    }
                    bot.sendMessage(chatID, greeting, {"parse_mode":"HTML"}).then(()=>{
                        logger.info(`sent message -> ${greeting}`);
                    }).catch((err)=>{
                        logger.info(`err -> ${err}`);
                    });
                }
                logger.log(`Ban operation success: ${result}`);
            } else if (isBannedWordUsedForUserNames(member)) {
                const result = bot.kickChatMember(MASTER_GROUP_CHAT_ID, member.id)
                if(result) {
                    logger.log(`kicked out a spammer ${result}`);
                } else {
                    logger.log(`Couldn't kick out a spammer ${err}`);
                }
            } else {
                if(PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER > 0) {
                    newUsersListForGreetingWithWelcomeMessages.push(`${member.first_name} [@${member.username}]`);
                }
            }
        }
    });

    if(PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER > 0 && newUsersListForGreetingWithWelcomeMessages.length >= PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER) {
        var listOfNamesToGreet = [];
        if (newUsersListForGreetingWithWelcomeMessages.length >= PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER) {
            listOfNamesToGreet = newUsersListForGreetingWithWelcomeMessages.splice(0, PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER);
        } else {
            listOfNamesToGreet = newUsersListForGreetingWithWelcomeMessages.splice(0, newUsersListForGreetingWithWelcomeMessages.length);
        }
        if(listOfNamesToGreet.length>0) {
            greetNewUsers(chatID, listOfNamesToGreet);
        }
    }
}

function greetNewUsers(chatID, newJoinerNames) {
    if(PREDEFINED_MSGS.WELCOME && PREDEFINED_MSGS.WELCOME.length>0) {
        var greeting = '';
        if(PREDEFINED_MSGS.WELCOME.search("{name}")>=0) {
            greeting = format(PREDEFINED_MSGS.WELCOME, {
                name: newJoinerNames.join(", ")
            });
        } else {
            greeting = PREDEFINED_MSGS.WELCOME;
        }
        bot.sendMessage(chatID, greeting, {"parse_mode":"HTML"}).then(()=>{
            logger.info(`sent message -> ${greeting}`);
        }).catch((err)=>{
            logger.info(`err -> ${err}`);
        });
    }
}

function treatKeyword(keyword) {
    var newKeyword = keyword;
    if(keyword.charAt(0)==='/') {
        newKeyword = keyword.substring(1, keyword.length);
    }
    if(newKeyword.indexOf('@') !== -1) {
        newKeyword =  newKeyword.substring(0, newKeyword.indexOf('@'))
    }
    return newKeyword;
}

function retrieveBannedWordsDictionary(){
    logger.info("retrieveBannedWordsDictionary: entry");
    if(!policingBannedWordsRetrievalComplete) {
        logger.info("Getting data");
        try{
            APIHelper.getBannedWordsDictionary().then((dictionary)=>{
                if(dictionary.data.length>0) {
                    bannedWordsForUserNamesScanEnabled = dictionary.data[0].enabled;
                    companyNameForBannedWordsForUserNamesScanning = dictionary.data[0].companyName.toLowerCase().split(',');
                    const bannedWords = dictionary.data[0].words.split(',');
                    _.each(bannedWords, (bannedWord)=>{
                        bannedWordsForUserNamesDictionary.add(bannedWord.trim().toLowerCase());
                    });
                } else {
                    logger.info("No Banned Words Found");
                }
            }).catch((err)=>{
                logger.info(err);
            });
        }catch(err) {
            logger.info(err);
        }
        policingBannedWordsRetrievalComplete = true;
    }
    logger.info("retrieveBannedWordsDictionary: exit");
}

function updateBannedWordsDictionary(dictionary) {
    logger.info("updateBannedWordsDictionary: entry");
    bannedWordsForUserNamesDictionary.clear();
    bannedWordsForUserNamesScanEnabled = dictionary.enabled;
    companyNameForBannedWordsForUserNamesScanning = dictionary.companyName.toLowerCase().split(',');
    const bannedWords = dictionary.words.split(',');
    _.each(bannedWords, (bannedWord)=>{
        bannedWordsForUserNamesDictionary.add(bannedWord.trim().toLowerCase());
    });
    logger.info("updateBannedWordsDictionary: exit");
}

function updateFriendlyGroupSettings(setting) {
    logger.info("updateFriendlyGroupSettings: entry");
    friendlyTelegramGroupsDictionary.clear();
    externalTelegramGroupsInviteScanEnabled = setting.enabled;
    const groupNames = setting.names.split(',');
    _.each(groupNames, (name)=>{
        friendlyTelegramGroupsDictionary.add(name.trim().toLowerCase());
    });
    PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE = setting.responseMsg && setting.responseMsg.length>0 ? setting.responseMsg : PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE;
    logger.info("updateFriendlyGroupSettings: entry");
}

function retrieveFriendlyGroupsSettings(){
    logger.info("retrieveFriendlyGroupsSettings: entry");
    if(!policingFriendlyGroupsRetrievalComplete) {
        logger.info("Getting data");
        try{
            APIHelper.getFriendlyGroupsSettings().then((setting)=>{
                if(setting.data.length>0) {
                    externalTelegramGroupsInviteScanEnabled = setting.data[0].enabled;
                    const groupNames = setting.data[0].names.split(',');
                    _.each(groupNames, (name)=>{
                        friendlyTelegramGroupsDictionary.add(name.trim().toLowerCase());
                    });
                    PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE = setting.data[0].responseMsg && setting.data[0].responseMsg.length>0 ? setting.data[0].responseMsg : PREDEFINED_MSGS.EXT_TELEGRAM_GROUP_INVITE_RESPONSE;
                } else {
                    logger.info("No Friendly Groups Settings Found");
                }
            }).catch((err)=>{
                logger.info(err);
            });
        }catch(err) {
            logger.info(err);
        }
        policingFriendlyGroupsRetrievalComplete = true;
    }
    logger.info("retrieveFriendlysGroupSettings: exit");
}

function deleteMsgFromScheduler(id) {
    logger.info("deleteMsgFromScheduler: entry");
    try{
        if(id && schedular.has(id.toString())) {
            logger.info("deleteMsgFromScheduler: stopping and removing scheduled message");
            clearInterval(schedular.get(id));
            schedular.delete(id);
        } else {
            logger.info("deleteMsgFromScheduler: process doesn't exist so cannot delete");
        }
    } catch (err) {
        logger.info("deleteMsgFromScheduler: error: couldn't stop and remove the scheduled messsage.");
    }
    logger.info("deleteMsgFromScheduler: exit");
}

function sendScheduledMessage(scheduledMsg) {
    logger.info("sendScheduledMessage: entry");
    if(bot && MASTER_GROUP_CHAT_ID) {
        bot.sendMessage(MASTER_GROUP_CHAT_ID, scheduledMsg.text, {"parse_mode":"HTML"}).then(()=>{
            logger.info("sendScheduledMessage: message sent");
        }).catch((err)=>{
            logger.info(`sendScheduledMessage: message couldn't be send - ${err}`);
        });
    } else {
        logger.info("sendScheduledMessage: message cannot be sent because either bot is not working or master group id doesn't exist.");
    }
    logger.info("sendScheduledMessage: exit");
}

function addNewMsgToScheduler(scheduledMsg) {
    logger.info("addNewMsgToScheduler: entry");
    const schedulerTimerInterval = parseInt(scheduledMsg.interval);
    logger.info(`addNewMsgToScheduler: Timer is set to ${schedulerTimerInterval / 1000 / 60} minutes`);
    if(schedular.has(scheduledMsg._id.toString())) {
        logger.info("addNewMsgToScheduler: process already running. Stopping it");
        deleteMsgFromScheduler(scheduledMsg._id);
    }
    if(_.isNaN(schedulerTimerInterval) || schedulerTimerInterval<=0) {
        logger.info("startMessageSchedular: Interval set to 0 so switching off the schedular");
    } else {
        logger.info("addNewMsgToScheduler: starting scheduler with timer " + schedulerTimerInterval);
        const scheduledMsgsProcess = setInterval(function() {
            sendScheduledMessage(scheduledMsg);
        }, schedulerTimerInterval); 
        schedular.add(scheduledMsgsProcess, scheduledMsg._id.toString());
    }
    logger.info("addNewMsgToScheduler: exit");
}

function retrieveScheduledMsgs(){
    logger.info("retrieveScheduledMsgs: entry");
    if(!retrieveScheduledMsgsComplete) {
        logger.info("Getting data");
        APIHelper.getScheduledMsgs().then((scheduledMsgs)=>{
            if(scheduledMsgs.data.length>0) {
                _.each(scheduledMsgs.data, (scheduledMsg)=>{
                    APIHelper.removeScheduledMsg(scheduledMsg._id).then(()=>{
                        delete scheduledMsg._id;
                        APIHelper.addScheduledMsg(scheduledMsg).then((response)=>{
                            logger.info("Existing scheduled message removed and a new one was created.");
                        }).catch((err)=>{
                            logger.info("Existing scheduled message removed but wasn't able to create a new one.");
                        });
                    }).catch((err)=>{
                        logger.info("Was not able to remove existing scheduled message.");
                    });
                });

            } else {
                logger.info("No scheduled messages found.");
            }
        }).catch((err)=>{
            logger.info(err);
        });
        retrieveScheduledMsgsComplete = true;
    }
    logger.info("retrieveScheduledMsgs: exit");
}

function updateBotSettings(settings) {
    logger.info("updateBotSettings : entry");
    if(settings) {
        logger.info("updateBotSettings : found settings. updating them.");
        PREDEFINED_MSGS.WELCOME = settings.welcomeMsg || "";
        PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER = settings.welcomeAggregatorNumber || 0;
        PREDEFINED_MSGS.DM_INVITE_MESSAGE = settings.helpMsg || "";
        PREDEFINED_MSGS.BOT_KICKED = settings.banMsg || "";
        PREDEFINED_MSGS.BOT_DM_HELP = settings.botMsgInDM || "";
        logger.info(`Settings updated with \nWELCOME: ${PREDEFINED_MSGS.WELCOME} \nDM_INVITE_MESSAGE: ${PREDEFINED_MSGS.DM_INVITE_MESSAGE} \nBOT_KICKED: ${PREDEFINED_MSGS.BOT_KICKED} \nBOT_DM_HELP: ${PREDEFINED_MSGS.BOT_DM_HELP}`);
        if(PREDEFINED_MSGS.WELCOME_AGGREGATER_NUMBER == 0) {
            newUsersListForGreetingWithWelcomeMessages.clear();
        }
    }
    logger.info("updateBotSettings : exit");
}

function retrieveBotSettings(){
    logger.info("retrieveBotSettings: entry");
    if(!settingBootupComplete) {
        APIHelper.getSettings().then((settings)=>{
            if(settings.data.length>0) {
                this.updateBotSettings(settings.data[0]);
            } else {
                logger.info("No saved settings found.");
            }
        }).catch((err)=>{
            logger.info(err);
        });
        settingBootupComplete = true;
    }
    logger.info("retrieveBotSettings: exit");
}

function getChannelAdmins() {
    logger.info("getChannelAdmins: entry");
    logger.info(MASTER_GROUP_USERNAME);
    bot.getChatAdministrators(MASTER_GROUP_USERNAME).then(admins => {
        const aList = [];
        _.each(admins, (admin)=>{
            aList.push(admin.user.username);
        });
        ADMINS = _.union(aList, PREDEFINED_ADMINS);
    });
    logger.info("getChannelAdmins: exit");
}

// just to keep this whole app alive
setInterval(function() {
    getChannelAdmins();
    https.get(SERVER_URL);
}, 300000); // every 5 minutes (300000)

module.exports = {
    bot: bot,

    retrieveBotSettings: retrieveBotSettings,
    
    retrieveScheduledMsgs: retrieveScheduledMsgs,
    addNewMsgToScheduler: addNewMsgToScheduler,
    deleteMsgFromScheduler: deleteMsgFromScheduler,
    
    updateBotSettings: updateBotSettings,

    retrieveBannedWordsDictionary: retrieveBannedWordsDictionary,
    updateBannedWordsDictionary: updateBannedWordsDictionary,

    retrieveFriendlyGroupsSettings: retrieveFriendlyGroupsSettings,
    updateFriendlyGroupSettings: updateFriendlyGroupSettings
};