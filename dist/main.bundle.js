webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container-fluid {\r\n    width: 100%;\r\n    padding: 0;\r\n    margin: 0;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n  <simple-notifications [options]=\"options\"></simple-notifications>\r\n  <app-navbar></app-navbar>\r\n  <router-outlet></router-outlet>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.options = {
            position: ["bottom", "right"],
            timeOut: 2000,
            lastOnBottom: true,
            showProgressBar: true,
            pauseOnHover: false
        };
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_emoji_picker__ = __webpack_require__("../../../../ng-emoji-picker/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_emoji_picker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ng_emoji_picker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/angular2-notifications.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_tabs__ = __webpack_require__("../../../../ngx-bootstrap/tabs/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_dropdown__ = __webpack_require__("../../../../ngx-bootstrap/dropdown/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__app_routes__ = __webpack_require__("../../../../../src/app/app.routes.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_LoginController__ = __webpack_require__("../../../../../src/app/services/LoginController.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_spapp_spapp_component__ = __webpack_require__("../../../../../src/app/components/spapp/spapp.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__components_navbar_navbar_component__ = __webpack_require__("../../../../../src/app/components/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__components_change_pwd_change_pwd_component__ = __webpack_require__("../../../../../src/app/components/change-pwd/change-pwd.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


















var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_spapp_spapp_component__["a" /* SpappComponent */],
                __WEBPACK_IMPORTED_MODULE_14__components_login_login_component__["a" /* LoginComponent */],
                __WEBPACK_IMPORTED_MODULE_15__components_navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_16__components_change_pwd_change_pwd_component__["a" /* ChangePwdComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["BrowserModule"],
                __WEBPACK_IMPORTED_MODULE_5_ng_emoji_picker__["EmojiPickerModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__["c" /* ModalModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_6_angular2_notifications__["SimpleNotificationsModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_8_ngx_bootstrap_tabs__["a" /* TabsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_7_ngx_bootstrap__["a" /* AlertModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_dropdown__["a" /* BsDropdownModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */],
                __WEBPACK_IMPORTED_MODULE_11__app_routes__["a" /* routing */]
            ],
            entryComponents: [__WEBPACK_IMPORTED_MODULE_16__components_change_pwd_change_pwd_component__["a" /* ChangePwdComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_12__services_LoginController__["a" /* LoginService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_10__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routes.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_login_login_component__ = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_spapp_spapp_component__ = __webpack_require__("../../../../../src/app/components/spapp/spapp.component.ts");



// Route Configuration
var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'spapp', component: __WEBPACK_IMPORTED_MODULE_2__components_spapp_spapp_component__["a" /* SpappComponent */] }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(routes);


/***/ }),

/***/ "../../../../../src/app/classes/bannedWordsForUserNames.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BannedWordsForUserNames; });
var BannedWordsForUserNames = (function () {
    function BannedWordsForUserNames() {
    }
    return BannedWordsForUserNames;
}());



/***/ }),

/***/ "../../../../../src/app/classes/botSettings.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BotSettings; });
var BotSettings = (function () {
    function BotSettings() {
    }
    return BotSettings;
}());



/***/ }),

/***/ "../../../../../src/app/classes/command.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Command; });
var Command = (function () {
    function Command() {
    }
    return Command;
}());



/***/ }),

/***/ "../../../../../src/app/classes/friendlyGroupsPolicingSettings.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FriendlyGroupsPolicingSettings; });
var FriendlyGroupsPolicingSettings = (function () {
    function FriendlyGroupsPolicingSettings() {
    }
    return FriendlyGroupsPolicingSettings;
}());



/***/ }),

/***/ "../../../../../src/app/classes/schedule_message.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScheduleMessage; });
var ScheduleMessage = (function () {
    function ScheduleMessage() {
    }
    return ScheduleMessage;
}());



/***/ }),

/***/ "../../../../../src/app/classes/user.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "../../../../../src/app/components/change-pwd/change-pwd.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/change-pwd/change-pwd.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"modal-header\">\r\n  <h4 class=\"modal-title pull-left\">Change Password</h4>\r\n</div>\r\n<div class=\"modal-body\">\r\n    <div class=\"form-horizontal\">\r\n        <div class=\"control-group\">\r\n            <label for=\"current_password\" class=\"control-label\">Current Password</label>\r\n            <div class=\"controls\">\r\n                <input name=\"current_password\" type=\"password\" [(ngModel)]=\"formPWDObj.current\" style=\"width: 100%\">\r\n            </div>\r\n        </div>\r\n        <div class=\"control-group\">\r\n            <label for=\"new_password\" class=\"control-label\">New Password</label>\r\n            <div class=\"controls\">\r\n                <input name=\"new_password\" type=\"password\" [(ngModel)]=\"formPWDObj.newPWD\" style=\"width: 100%\">\r\n            </div>\r\n        </div>\r\n        <div class=\"control-group\">\r\n            <label for=\"confirm_password\" class=\"control-label\">Confirm Password</label>\r\n            <div class=\"controls\">\r\n                <input name=\"confirm_password\" type=\"password\" [(ngModel)]=\"formPWDObj.newPWD1\" style=\"width: 100%\">\r\n            </div>\r\n        </div>\r\n        <p></p>\r\n        <div class=\"control-group\">\r\n          <span class=\"text-danger\">{{errText}}</span>\r\n        </div>\r\n    </div>\r\n</div>\r\n<div class=\"modal-footer\">\r\n  <a class=\"btn btn-primary\" (click)=\"savePassword()\" id=\"password_modal_save\">Save Changes</a>\r\n  <a class=\"btn btn-mute\" (click)=\"bsModalRef.hide()\">Exit</a>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/change-pwd/change-pwd.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePwdComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal_bs_modal_ref_service__ = __webpack_require__("../../../../ngx-bootstrap/modal/bs-modal-ref.service.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_LoginController__ = __webpack_require__("../../../../../src/app/services/LoginController.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/angular2-notifications.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_notifications__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ChangePwdComponent = (function () {
    function ChangePwdComponent(_LoginService, bsModalRef, _notificationService) {
        this._LoginService = _LoginService;
        this.bsModalRef = bsModalRef;
        this._notificationService = _notificationService;
        this.formPWDObj = {
            current: "",
            newPWD: "",
            newPWD1: ""
        };
        this.errText = "";
    }
    ChangePwdComponent.prototype.ngOnInit = function () {
    };
    ChangePwdComponent.prototype.savePassword = function () {
        var _this = this;
        this.errText = "";
        if (this.formPWDObj.current != this._LoginService.currentLoggedInUser().password) {
            this.errText = "Please provide your previous password";
            return;
        }
        if (this.formPWDObj.newPWD != this.formPWDObj.newPWD1) {
            this.errText = "Provided passwords doesn't match";
            return;
        }
        this._LoginService.changePWD(this.formPWDObj.newPWD).then(function (response) {
            _this.bsModalRef.hide();
            _this._notificationService.success("success", "Password Changed");
        }).catch(function (err) {
            _this.bsModalRef.hide();
            _this._notificationService.error("error", "Couldn't Change Password");
        });
    };
    ChangePwdComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'change-pwd',
            template: __webpack_require__("../../../../../src/app/components/change-pwd/change-pwd.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/change-pwd/change-pwd.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_LoginController__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_1_ngx_bootstrap_modal_bs_modal_ref_service__["a" /* BsModalRef */],
            __WEBPACK_IMPORTED_MODULE_3_angular2_notifications__["NotificationsService"]])
    ], ChangePwdComponent);
    return ChangePwdComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".wrapper {    \r\n\tmargin-top: 80px;\r\n\tmargin-bottom: 20px;\r\n}\r\n\r\n.form-signin {\r\n  max-width: 420px;\r\n  padding: 30px 38px 66px;\r\n  margin: 0 auto;\r\n  background-color: #eee;\r\n  border: 3px dotted rgba(0,0,0,0.1);  \r\n  }\r\n\r\n.form-signin-heading {\r\n  text-align:center;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.form-control {\r\n  position: relative;\r\n  font-size: 16px;\r\n  height: auto;\r\n  padding: 10px;\r\n}\r\n\r\ninput[type=\"text\"] {\r\n  margin-bottom: 0px;\r\n  border-bottom-left-radius: 0;\r\n  border-bottom-right-radius: 0;\r\n}\r\n\r\ninput[type=\"password\"] {\r\n  margin-bottom: 20px;\r\n  border-top-left-radius: 0;\r\n  border-top-right-radius: 0;\r\n}\r\n\r\n.colorgraph {\r\n  height: 7px;\r\n  border-top: 0;\r\n  background: #c4e17f;\r\n  border-radius: 5px;\r\n  background-image: -webkit-gradient(linear, left top, right top, from(#c4e17f), color-stop(12.5%, #c4e17f), color-stop(12.5%, #f7fdca), color-stop(25%, #f7fdca), color-stop(25%, #fecf71), color-stop(37.5%, #fecf71), color-stop(37.5%, #f0776c), color-stop(50%, #f0776c), color-stop(50%, #db9dbe), color-stop(62.5%, #db9dbe), color-stop(62.5%, #c49cde), color-stop(75%, #c49cde), color-stop(75%, #669ae1), color-stop(87.5%, #669ae1), color-stop(87.5%, #62c2e4), to(#62c2e4));\r\n  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class = \"container\">\r\n\t<div class=\"wrapper\">\r\n\t\t<form action=\"\" method=\"post\" name=\"Login_Form\" class=\"form-signin\">       \r\n\t\t    <h3 class=\"form-signin-heading\">Welcome Back! Please Sign In</h3>\r\n\t\t\t<hr class=\"colorgraph\"><br>\r\n\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Username\" required=\"\" autofocus=\"\" name=\"username\" id=\"username\" [(ngModel)]=\"userObj.username\" />\r\n\t\t\t<input type=\"password\" class=\"form-control\" placeholder=\"Password\" required=\"\" name=\"password\" id=\"password\" [(ngModel)]=\"userObj.password\"/>\r\n\t\t\t<button class=\"btn btn-lg btn-primary btn-block\" name=\"Submit\" value=\"Login\" (click)=\"doLogin()\">Login</button>  \t\t\t\r\n\t    </form>\r\n\t</div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_user__ = __webpack_require__("../../../../../src/app/classes/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/angular2-notifications.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_LoginController__ = __webpack_require__("../../../../../src/app/services/LoginController.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = (function () {
    function LoginComponent(_notificationService, _router, _LoginService) {
        this._notificationService = _notificationService;
        this._router = _router;
        this._LoginService = _LoginService;
        this.userObj = new __WEBPACK_IMPORTED_MODULE_2__classes_user__["a" /* User */]();
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.doLogin = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_0_lodash__["isEmpty"](this.userObj.username) || __WEBPACK_IMPORTED_MODULE_0_lodash__["isEmpty"](this.userObj.password)) {
            this._notificationService.error("Error", "Username and password cannot be empty");
            return;
        }
        this._LoginService.doLogin(this.userObj)
            .then(function (response) {
            if (!response) {
                _this._notificationService.error("Error", "Login failed.");
            }
            else {
                _this._router.navigate(['spapp']);
            }
        }).catch(function (err) {
            _this._notificationService.error("Error", "Login failed.");
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'app-login',
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angular2_notifications__["NotificationsService"],
            __WEBPACK_IMPORTED_MODULE_5__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_4__services_LoginController__["a" /* LoginService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "a.navbar-brand,\r\n.nav-item {\r\n    color: #fff;\r\n    font-weight: bold;\r\n}\r\n\r\n.nav-bar-contents {\r\n    width: 100%;\r\n    padding: 0;\r\n    margin: 0;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-dark bg-dark justify-content-between\">\r\n  <a class=\"navbar-brand\">Telegram BOT Management</a>\r\n  <div *ngIf=\"isLoggedIn()\">\r\n  <!-- <div> -->\r\n    <a class=\"btn\" (click)=\"changePWD()\"><i class=\"fa fa-user fa-2x nav-item\" style=\"float: right\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Change Password\"></i></a>\r\n    <a class=\"btn\" (click)=\"logout()\"><i class=\"fa fa-sign-out fa-2x nav-item\" aria-hidden=\"true\" data-toggle=\"tooltip\" data-placement=\"bottom\" title=\"Logout\"></i></a>\r\n  </div>\r\n</nav>"

/***/ }),

/***/ "../../../../../src/app/components/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_LoginController__ = __webpack_require__("../../../../../src/app/services/LoginController.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__change_pwd_change_pwd_component__ = __webpack_require__("../../../../../src/app/components/change-pwd/change-pwd.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var NavbarComponent = (function () {
    function NavbarComponent(_LoginService, modalService) {
        this._LoginService = _LoginService;
        this.modalService = modalService;
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent.prototype.isLoggedIn = function () {
        return this._LoginService.isUserLoggedIn();
    };
    NavbarComponent.prototype.logout = function () {
        this._LoginService.logout();
    };
    NavbarComponent.prototype.changePWD = function () {
        this.bsModalRef = this.modalService.show(__WEBPACK_IMPORTED_MODULE_3__change_pwd_change_pwd_component__["a" /* ChangePwdComponent */]);
        this.bsModalRef.content.closeBtnName = 'Close';
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/components/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_LoginController__["a" /* LoginService */],
            __WEBPACK_IMPORTED_MODULE_2_ngx_bootstrap__["b" /* BsModalService */]])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/spapp/spapp.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".contents {\r\n    margin: 16px;\r\n}\r\n\r\n.tabContainer {\r\n    margin: 16px;\r\n}\r\n\r\n.emptyState {\r\n    width: 100%;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n}\r\n\r\n.commandCardsSection {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-wrap: wrap;\r\n        flex-wrap: wrap;\r\n    -webkit-box-pack: justify;\r\n        -ms-flex-pack: justify;\r\n            justify-content: space-between;\r\n}\r\n\r\n.commandCard{\r\n    width: auto;\r\n    -ms-flex-wrap: wrap;\r\n        flex-wrap: wrap;\r\n    min-width: 18rem;\r\n    border: 1px solid #212529;\r\n    margin: 0 25px 15px 0;\r\n    border-radius: 25px;\r\n}\r\n\r\n.sampleCommandCardsSection {\r\n    display: -webkit-box;\r\n    display: -ms-flexbox;\r\n    display: flex;\r\n    -ms-flex-wrap: wrap;\r\n        flex-wrap: wrap;\r\n    -webkit-box-pack: center;\r\n        -ms-flex-pack: center;\r\n            justify-content: center;\r\n}\r\n\r\n.sampleCommandCard{\r\n    width: 18rem;\r\n    border: 1px solid #212529;\r\n    margin-bottom: 15px;\r\n    border-radius: 25px;\r\n}\r\n\r\n.inputAreaInput {\r\n    border: 0px solid;\r\n    width: 100%;\r\n    margin-bottom: 2px;\r\n}\r\n\r\n.textAreaInput {\r\n    border: 0px solid;\r\n}\r\n\r\n.emojiSearch {\r\n    padding: 0;\r\n}\r\n\r\n.emojiSearch .search-header {\r\n    background-color: green !important;\r\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/spapp/spapp.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row contents\">\r\n  <tabset [justified]=\"true\" style=\"width: 100%\">\r\n    <tab heading='Bot Commands'>\r\n      <div class=\"tabContainer\">\r\n        <div class=\"row\">\r\n          <tabset type=\"pills\" style=\"width: 100%\">\r\n              <tab heading='Existing Commands'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row\">\r\n                    <p><br></p>\r\n                    <div *ngIf=\"commands.length<=0\" class=\"emptyState\">\r\n                      <div class=\"jumbotron\">\r\n                        <h1>No commands exist in the system.</h1>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"commandCardsSection\">\r\n                      <div\r\n                        *ngFor=\"let command of commands;\"\r\n                        (click)=\"selectCommand(command)\"\r\n                        [class.active]=\"command === selectedCommand\">\r\n          \r\n                        <div class=\"card\" class=\"commandCard\">\r\n                          <div class=\"card-body\">\r\n                            <h5 class=\"card-title\">!{{command.name}}</h5>\r\n                            <p class=\"card-text\">{{command.details}}</p>\r\n                            <button (click)=\"deleteCommand(command._id)\" class=\"btn btn-sm btn-danger\">Delete</button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n              \r\n              <tab heading='Add new Command'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row formSection\">\r\n                    <div class=\"col-sm-7 jumbotron\">\r\n                      <form>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"command_name\">Command Name</label>\r\n                          <input type=\"text\" class=\"form-control\" id=\"command_name\" name=\"command_name\" [(ngModel)]=\"newCommand.name\" placeholder=\"Command name in lowercase without /\">\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"command_details\">Response Text</label>\r\n                          <emoji-input \r\n                            #commandInput\r\n                            class=\"form-control\"\r\n                            id=\"command_details\" \r\n                            name=\"command_details\"\r\n                            [(model)]=\"newCommand.details\"\r\n                            [textArea]=\"{cols: 95, rows: 4}\"\r\n                            [inputClass]=\"'textAreaInput'\"\r\n                            [searchClass]=\"'emojiSearch'\"\r\n                            (setPopupAction)=\"setPopupActionAddCmd($event)\">\r\n                          </emoji-input>\r\n                          <a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupAddCmd(true)\"><li class=\"fa fa-smile-o\"></li></a>\r\n                        </div>\r\n                          <button (click)=\"addNewCommand()\" class=\"btn btn-success\">Submit</button>\r\n                          <button (click)=\"resetNewCommandForm()\" class=\"btn btn-danger\">Cancel</button>\r\n                      </form>\r\n                    </div>\r\n                    <div class=\"col-sm-5\">\r\n                      <div class=\"sampleCommandCardsSection\">\r\n                        <div class=\"card\" class=\"sampleCommandCard\">\r\n                          <div class=\"card-body\">\r\n                            <h5 class=\"card-title\">/{{newCommand.name}}</h5>\r\n                            <p class=\"card-text\">{{newCommand.details}}</p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n            </tabset>\r\n        </div>\r\n      </div>\r\n    </tab>\r\n    <tab heading='Scheduled Messages'>\r\n      <div class=\"tabContainer\">\r\n        <div class=\"row\">\r\n            <tabset type=\"pills\" style=\"width: 100%\" #scheduledMsgsTabs>\r\n              <tab heading='Existing Messages'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row\">\r\n                    <p><br></p>\r\n                    <div *ngIf=\"scheduledMsgs.length<=0\" class=\"emptyState\">\r\n                      <div class=\"jumbotron\">\r\n                        <h1>No existing messages exist in the system.</h1>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"commandCardsSection\">\r\n                      <div\r\n                        *ngFor=\"let scheduledMsg of scheduledMsgs;\"\r\n                        (click)=\"selectScheduledMsg(scheduledMsg)\"\r\n                        [class.active]=\"scheduledMsg === selectedScheduledMsg\">\r\n          \r\n                        <div class=\"card\" class=\"commandCard\">\r\n                          <div class=\"card-body\">\r\n                            <h4 class=\"card-title\">{{composeTimerDropdownMsg(scheduledMsg.interval)}}</h4>\r\n                            <p class=\"card-text\">{{scheduledMsg.text}}</p>\r\n                            <button (click)=\"editScheduledMsg(scheduledMsg._id)\" class=\"btn btn-sm btn-warning card-link\">Edit</button>\r\n                            <button (click)=\"deleteScheduledMsg(scheduledMsg._id)\" class=\"btn btn-sm btn-danger card-link\">Delete</button>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n              \r\n              <tab heading='{{addNewMsgTabHeader}}'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row formSection\">\r\n                    <div class=\"col-sm-7 jumbotron\">\r\n                      <form>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"message_text\">Text</label>\r\n                          <emoji-input \r\n                            #scheduledMsgInput\r\n                            class=\"form-control\"\r\n                            id=\"message_text\" \r\n                            name=\"message_text\"\r\n                            [(model)]=\"newScheduledMsg.text\"\r\n                            [textArea]=\"{cols: 95, rows: 4}\"\r\n                            [inputClass]=\"'textAreaInput'\"\r\n                            [searchClass]=\"'emojiSearch'\"\r\n                            (setPopupAction)=\"setPopupActionNewScheduledMsg($event)\">\r\n                          </emoji-input>\r\n                          <a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupNewScheduledMsg(true)\"><li class=\"fa fa-smile-o\"></li></a>\r\n\r\n                          <!-- <textarea class=\"form-control\" rows=\"9\" id=\"message_text\" name=\"message_text\"  [(ngModel)]=\"newScheduledMsg.text\"></textarea> -->\r\n                        </div>\r\n                        <div class=\"btn-group\" dropdown>\r\n                          <button type=\"button\" class=\"btn btn-md btn-primary\">{{composeTimerDropdownMsg(newScheduledMsg.interval)}}</button>\r\n                          <button type=\"button\" dropdownToggle class=\"btn btn-md btn-primary dropdown-toggle dropdown-toggle-split\">\r\n                            <span class=\"caret\"></span>\r\n                          </button>\r\n                          <ul *dropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(0)\">turn off</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1)\">1 minute</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(5)\">5 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(10)\">10 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(15)\">15 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(20)\">20 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(25)\">25 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(30)\">30 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(35)\">35 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(40)\">40 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(45)\">45 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(50)\">50 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(55)\">55 minutes</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(60)\">1 hour</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(90)\">1 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(120)\">2 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(150)\">2 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(180)\">3 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(210)\">3 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(240)\">4 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(270)\">4 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(300)\">5 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(330)\">5 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(360)\">6 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(390)\">6 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(420)\">7 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(450)\">7 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(480)\">8 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(510)\">8 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(540)\">9 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(570)\">9 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(600)\">10 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(630)\">10 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(660)\">11 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(690)\">11 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(780)\">12 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(750)\">12 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(780)\">13 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(810)\">13 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(840)\">14 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(870)\">14 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(900)\">15 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(930)\">15 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(960)\">16 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(990)\">16 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1020)\">17 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1050)\">17 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1080)\">18 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1110)\">18 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1140)\">19 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1170)\">19 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1200)\">20 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1230)\">20 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1260)\">21 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1290)\">21 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1320)\">22 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1350)\">22 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1380)\">23 hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1410)\">23 and a half hours</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(1440)\">1 day</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(2160)\">1 and a half days</a></li>\r\n                            <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setSchedulerTimer(2880)\">2 day</a></li>\r\n                          </ul>\r\n                        </div>\r\n                        <div style=\"float: right;\">\r\n                          <a class=\"btn btn-success\" *ngIf=\"saveScheduledMsgButonEnabled()\" (click)=\"addNewScheduledMsg()\">Save</a>\r\n                          <button class=\"btn btn-muted\" *ngIf=\"!saveScheduledMsgButonEnabled()\">Save</button>\r\n                          <button type=\"reset\" class=\"btn btn-danger\" (click)=\"resetNewScheduledMsgForm()\">Cancel</button>\r\n                        </div>\r\n                      </form>\r\n                    </div>\r\n                    <div class=\"col-sm-5\">\r\n                      <div class=\"sampleCommandCardsSection\">\r\n                        <div class=\"card\" class=\"sampleCommandCard\" *ngIf=\"newScheduledMsg.text && newScheduledMsg.text.length>0\">\r\n                          <div class=\"card-body\">\r\n                            <h4 class=\"card-title\">{{composeTimerDropdownMsg(newScheduledMsg.interval)}}</h4>\r\n                            <p class=\"card-text\">{{newScheduledMsg.text}}</p>\r\n                          </div>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n\r\n              <tab [disabled]='true' customClass=\"myClass\">\r\n                  <ng-template tabHeading>\r\n                  </ng-template>\r\n              </tab>\r\n            </tabset>\r\n        </div>\r\n      </div>\r\n    </tab>\r\n\r\n    <tab heading='Bot Settings'>\r\n      <div class=\"tabContainer\">\r\n        <div class=\"row\">\r\n            <tabset type=\"pills\" style=\"width: 100%\">\r\n              <tab heading='Bot General Response'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row\">\r\n                      <form style=\"width: 100%\">\r\n                        <div class=\"card\">\r\n                          <div class=\"card-header\">\r\n                            <strong>Welcome Message</strong>\r\n                          </div>\r\n                          <div class=\"card-block\" style=\"padding: 15px;\">\r\n                            <div class=\"card-text\" style=\"margin-bottom: 22px;\">\r\n                              <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                                  <emoji-input \r\n                                    #welcomeMsg\r\n                                    class=\"form-control\"\r\n                                    id=\"welcomeMsg\"\r\n                                    name=\"welcomeMsg\"\r\n                                    [textArea]=\"{cols: 95, rows: 4}\"\r\n                                    [(model)]=\"settings.welcomeMsg\"\r\n                                    [inputClass]=\"'inputAreaInput'\"\r\n                                    [searchClass]=\"'emojiSearch'\"\r\n                                    (setPopupAction)=\"setPopupActionWelcomeMsg($event)\">\r\n                                  </emoji-input>\r\n                                <div class=\"input-group-addon\"><a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupWelcomeMsg(true)\"><li class=\"fa fa-smile-o\"></li></a></div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">This message will appear when a new user join. Leave blank if you don't want to great users.</div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">Add &#123;name&#125; between the text where you want new user's name to appear. For example: Hi &#123;name&#125;. Thank you for joining us.</div>\r\n                            <div class=\"card-subtitle mb-2\">\r\n                              <div class=\"btn-group\" dropdown>\r\n                                <button type=\"button\" class=\"btn btn-md btn-primary\">{{composeAggregatorDropdownMsg(settings.welcomeAggregatorNumber)}}</button>\r\n                                <button type=\"button\" dropdownToggle class=\"btn btn-md btn-primary dropdown-toggle dropdown-toggle-split\">\r\n                                  <span class=\"caret\"></span>\r\n                                </button>\r\n                                <ul *dropdownMenu class=\"dropdown-menu\" role=\"menu\">\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(0)\">Stop Greeting</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(1)\">Greet Every User</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(5)\">Greet 5 Users Together</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(10)\">Greet 10 Users Together</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(15)\">Greet 15 Users Together</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(20)\">Greet 20 Users Together</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(25)\">Greet 25 users Together</a></li>\r\n                                  <li role=\"menuitem\"><a class=\"dropdown-item\" (click)=\"setAggregatedUserValuesForWelcomeMessage(30)\">Greet 30 users Together</a></li>\r\n                                </ul>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"card\" style=\"margin-top: 15px;\">\r\n                          <div class=\"card-header\">\r\n                            <strong>Bot Response in DM</strong>\r\n                          </div>\r\n                          <div class=\"card-block\" style=\"padding: 15px;\">\r\n                            <div class=\"card-text\" style=\"margin-bottom: 22px;\">\r\n                              <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                                  <emoji-input \r\n                                    #dmResponseMsg\r\n                                    class=\"form-control\"\r\n                                    id=\"dmResponseMsg\"\r\n                                    name=\"dmResponseMsg\"\r\n                                    [(model)]=\"settings.botMsgInDM\"\r\n                                    [textArea]=\"{cols: 95, rows: 4}\"\r\n                                    [inputClass]=\"'inputAreaInput'\"\r\n                                    [searchClass]=\"'emojiSearch'\"\r\n                                    (setPopupAction)=\"setPopupActionBotMsgInDM($event)\">\r\n                                  </emoji-input>\r\n                                <div class=\"input-group-addon\"><a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupBotMsgInDM(true)\"><li class=\"fa fa-smile-o\"></li></a></div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">The bot will send this message in DM when user clicks on the help command. You cannot leave this blank. Addition of &#123;commands&#125; is compulsary</div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">You have to Add &#123;commands&#125; in the text where you want the commands to appear. For example: Hey User, You can ask me &#123;commands&#125;.</div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"card\" style=\"margin-top: 15px;\">\r\n                          <div class=\"card-header\">\r\n                            <strong>Banning Message</strong>\r\n                          </div>\r\n                          <div class=\"card-block\" style=\"padding: 15px;\">\r\n                            <div class=\"card-text\" style=\"margin-bottom: 22px;\">\r\n                              <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                                  <emoji-input \r\n                                    #bannedMsg\r\n                                    class=\"form-control\"\r\n                                    id=\"bannedMsg\"\r\n                                    name=\"bannedMsg\"\r\n                                    [(model)]=\"settings.banMsg\"\r\n                                    [inputClass]=\"'inputAreaInput'\"\r\n                                    [textArea]=\"{cols: 95, rows: 4}\"\r\n                                    [searchClass]=\"'emojiSearch'\"\r\n                                    (setPopupAction)=\"setPopupActionBanMsg($event)\">\r\n                                  </emoji-input>\r\n                                <div class=\"input-group-addon\"><a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupBanMsg(true)\"><li class=\"fa fa-smile-o\"></li></a></div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">This message will appear when a bot has been banned. Leave blank if you don't want the message to appear. </div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">Add &#123;name&#125; between the text where you want banned user's name to appear. For example: &#123;name&#125; was banned as it was a bot.</div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"card\" style=\"margin-top: 15px;\">\r\n                          <div class=\"card-header\">\r\n                            <strong>Help Command Response Message</strong>\r\n                          </div>\r\n                          <div class=\"card-block\" style=\"padding: 15px;\">\r\n                            <div class=\"card-text\" style=\"margin-bottom: 22px;\">\r\n                              <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                                  <emoji-input \r\n                                    #helpMsg\r\n                                    class=\"form-control\"\r\n                                    id=\"helpMsg\"\r\n                                    name=\"helpMsg\"\r\n                                    [(model)]=\"settings.helpMsg\"\r\n                                    [textArea]=\"{cols: 95, rows: 4}\"\r\n                                    [inputClass]=\"'inputAreaInput'\"\r\n                                    [searchClass]=\"'emojiSearch'\"\r\n                                    (setPopupAction)=\"setPopupActionHelpMsg($event)\">\r\n                                  </emoji-input>\r\n                                <div class=\"input-group-addon\"><a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupHelpMsg(true)\"><li class=\"fa fa-smile-o\"></li></a></div>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">This message will appears when a user clicks on the help command. Bot will always engage user in a DM. Leave blank if you don't want any message to appear.</div>\r\n                            <div class=\"card-subtitle mb-2 text-muted\">Add &#123;name&#125; between the text where you want user's name to appear. For example: Hey &#123;name&#125;. Lets take this to a DM.</div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"card\" style=\"margin-top: 15px;\">\r\n                          <div class=\"card-block\" style=\"padding: 15px;\">\r\n                            <a  class=\"btn btn-success\" *ngIf=\"saveButonEnabled()\" (click)=\"saveSettings()\">Save Changes</a>\r\n                            <button class=\"btn btn-muted\" *ngIf=\"!saveButonEnabled()\">Save Changes</button>\r\n                          </div>\r\n                        </div>\r\n                      </form>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n              \r\n              <tab heading='Banned Words for Channel Member Names'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-lg-12 jumbotron\">\r\n                      <form>\r\n                        <div class=\"form-group\">\r\n                          <div class=\"form-check form-check-inline\">\r\n                            <label class=\"form-check-label\">\r\n                              <input class=\"form-check-input\" type=\"checkbox\" id=\"inlineCheckbox1\" [checked]=\"bannedWordForUserNamesSettings.enabled\" (change)=\"bannedWordForUserNamesSettings.enabled = !bannedWordForUserNamesSettings.enabled\"> Enable Scanning of bad words and upon matching banning users\r\n                            </label>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label for=\"companyName\">Your Brand Name</label>\r\n                            <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                              <input required type=\"text\" class=\"form-control\" id=\"companyName\" name=\"companyName\" [(ngModel)]=\"bannedWordForUserNamesSettings.companyName\" placeholder=\"Brand Name\">\r\n                            </div>\r\n                            <div class=\"text-muted font-italic\">You have to name channel admins as \"Admin - Brand Name\". For example \"Admin - Ethereum Group\". if more than one brand name then seperate it by a comma. BrandA, BrandB</div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"bannedWords\">Banned words</label>\r\n                          <textarea class=\"form-control\" rows=\"3\" id=\"bannedWords\" name=\"bannedWords\" [(ngModel)]=\"bannedWordForUserNamesSettings.words\"></textarea>\r\n                          <div class=\"text-muted font-italic\">Users cannot have these words in their names. Enter words seperated by comma. For example:. Admin, Support, Wallet, ICO</div>\r\n                        </div>\r\n                        <a  class=\"btn btn-success\" *ngIf=\"bannedWordForUserNamesSettingsSaveButonEnabled()\" (click)=\"saveBannedWordsForUserNamesSettings()\">Save Changes</a>\r\n                        <button class=\"btn btn-muted\" *ngIf=\"!bannedWordForUserNamesSettingsSaveButonEnabled()\">Save Changes</button>\r\n                      </form>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n              \r\n              <tab heading='External Groups Invite Policy'>\r\n                <div class=\"tabContainer\">\r\n                  <div class=\"row\">\r\n                    <div class=\"col-lg-12 jumbotron\">\r\n                      <form>\r\n                        <div class=\"form-group\">\r\n                          <div class=\"form-check form-check-inline\">\r\n                            <label class=\"form-check-label\">\r\n                              <input class=\"form-check-input\" type=\"checkbox\" id=\"inlineCheckbox1\" [checked]=\"friendlyGroupsPolicingSettings.enabled\" (change)=\"friendlyGroupsPolicingSettings.enabled = !friendlyGroupsPolicingSettings.enabled\"> Enable Removal of Invites to External Links\r\n                            </label>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                            <label for=\"inviteResponse\">Response Message</label>\r\n                            <div class=\"input-group mb-2 mr-sm-2 mb-sm-0\">\r\n                              <emoji-input \r\n                                #inviteResponse\r\n                                class=\"form-control\"\r\n                                id=\"inviteResponse\"\r\n                                name=\"inviteResponse\"\r\n                                [textArea]=\"{cols: 95, rows: 4}\"\r\n                                [(model)]=\"friendlyGroupsPolicingSettings.responseMsg\"\r\n                                [inputClass]=\"'inputAreaInput'\"\r\n                                [searchClass]=\"'emojiSearch'\"\r\n                                (setPopupAction)=\"setPopupActionFriendlyGroupsPolicingSettings($event)\">\r\n                              </emoji-input>\r\n                              <div class=\"input-group-addon\"><a name=\"button\" class=\"btn btn-mute\" (click)=\"openPopupFriendlyGroupsPolicingSettings(true)\"><li class=\"fa fa-smile-o\"></li></a></div>\r\n                            </div>\r\n                            <div class=\"text-muted font-italic\">Add &#123;name&#125; between the text where you want the user's name to appear. For example: Hey &#123;name&#125;, please don't post external group's invites.</div>\r\n                        </div>\r\n                        <div class=\"form-group\">\r\n                          <label for=\"friendlyGroupsNames\">Friendly Groups' Names</label>\r\n                          <textarea class=\"form-control\" rows=\"3\" id=\"friendlyGroupsNames\" name=\"friendlyGroupsNames\" [(ngModel)]=\"friendlyGroupsPolicingSettings.names\"></textarea>\r\n                          <div class=\"text-muted font-italic\">Enter group names seperated by comma. You can get the group name from invite link.<br>e.g if the invite links are t.me/myGroup1 and t.me/myGroup2 then enter myGroup1, myGroup2.</div>\r\n                        </div>\r\n                        <button (click)=\"saveFriendlyGroupsPolicingSettings()\" class=\"btn btn-success\">Save</button>\r\n                      </form>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </tab>\r\n\r\n            </tabset>\r\n        </div>\r\n      </div>\r\n    </tab>\r\n  </tabset>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/spapp/spapp.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpappComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash__ = __webpack_require__("../../../../lodash/lodash.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_lodash__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__("../../../../axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications__ = __webpack_require__("../../../../angular2-notifications/angular2-notifications.umd.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_notifications___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_notifications__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__ = __webpack_require__("../../../../ngx-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__("../../../../moment/moment.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__classes_command__ = __webpack_require__("../../../../../src/app/classes/command.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__classes_schedule_message__ = __webpack_require__("../../../../../src/app/classes/schedule_message.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__classes_botSettings__ = __webpack_require__("../../../../../src/app/classes/botSettings.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__classes_friendlyGroupsPolicingSettings__ = __webpack_require__("../../../../../src/app/classes/friendlyGroupsPolicingSettings.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__classes_bannedWordsForUserNames__ = __webpack_require__("../../../../../src/app/classes/bannedWordsForUserNames.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_LoginController__ = __webpack_require__("../../../../../src/app/services/LoginController.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var SpappComponent = (function () {
    function SpappComponent(_service, _LoginService) {
        var _this = this;
        this._service = _service;
        this._LoginService = _LoginService;
        // private serverURL = 'https://tbot-demo.herokuapp.com';
        // private commandsURL = this.serverURL.concat('/commands');
        // private scheduledMsgsURL = this.serverURL.concat('/scheduled');
        // private settingsURL = this.serverURL.concat('/settings');
        // private bannedWordsForUserNamePoliceURL = this.serverURL.concat('/police/bannedwords');
        // private friendlyGroupsPoliceURL = this.serverURL.concat('/police/friendlygroups');
        this.commandsURL = '/commands';
        this.scheduledMsgsURL = '/scheduled';
        this.settingsURL = '/settings';
        this.bannedWordsForUserNamePoliceURL = '/police/bannedwords';
        this.friendlyGroupsPoliceURL = '/police/friendlygroups';
        this.friendlyGroupsPolicingSettings = new __WEBPACK_IMPORTED_MODULE_9__classes_friendlyGroupsPolicingSettings__["a" /* FriendlyGroupsPolicingSettings */]();
        this.bannedWordForUserNamesSettings = new __WEBPACK_IMPORTED_MODULE_10__classes_bannedWordsForUserNames__["a" /* BannedWordsForUserNames */]();
        ////////////////////////////// Commands /////////////////////////////////////////////////////
        this.commands = [];
        this.getIndexOfCommand = function (commandId) {
            return _this.commands.findIndex(function (command) { return command._id === commandId; });
        };
        //////////////////////////////// Settings //////////////////////////////////////////////////////////////
        this.settings = new __WEBPACK_IMPORTED_MODULE_8__classes_botSettings__["a" /* BotSettings */]();
        this.scheduledMsgs = [];
        this.addNewMsgTabHeader = "Add new Message";
        this.getIndexOfScheduledMsg = function (scheduledMsgId) {
            return _this.scheduledMsgs.findIndex(function (scheduledMsg) { return scheduledMsg._id === scheduledMsgId; });
        };
    }
    SpappComponent.prototype.ngOnInit = function () {
        this._LoginService.isUserAuthorized();
        this.getCommands();
        this.newCommand = new __WEBPACK_IMPORTED_MODULE_6__classes_command__["a" /* Command */]();
        this.getScheduledMsgs();
        this.newScheduledMsg = new __WEBPACK_IMPORTED_MODULE_7__classes_schedule_message__["a" /* ScheduleMessage */]();
        this.getSettings();
        this.getBannedWordsForUserNamesSettings();
        this.getFriendlyGroupsPolicingSettings();
    };
    SpappComponent.prototype.setPopupActionFriendlyGroupsPolicingSettings = function (fn) {
        this.openPopupFriendlyGroupsPolicingSettings = fn;
    };
    SpappComponent.prototype.getFriendlyGroupsPolicingSettings = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(this.friendlyGroupsPoliceURL)
            .then(function (settings) {
            if (settings.data && settings.data.length > 0) {
                settings.data.forEach(function (setting) {
                    _this.friendlyGroupsPolicingSettings._id = setting._id;
                    _this.friendlyGroupsPolicingSettings.enabled = setting.enabled;
                    _this.friendlyGroupsPolicingSettings.names = setting.names;
                    _this.friendlyGroupsPolicingSettings.responseMsg = setting.responseMsg;
                });
            }
            else {
                console.log('Nothing retrieved');
            }
        }).catch(function (err) {
            _this._service.error("Error", "Failed to retrieve friendly groups settings");
            console.log('Nothing retrieved. Error: ' + err);
        });
    };
    SpappComponent.prototype.saveFriendlyGroupsPolicingSettings = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post(this.friendlyGroupsPoliceURL, this.friendlyGroupsPolicingSettings)
            .then(function (response) {
            if (response.data) {
                _this.friendlyGroupsPolicingSettings._id = response.data._id;
                _this._service.success("Success", "Settings saved!");
            }
        }).catch(function (err) {
            console.log(err);
            _this._service.error("Error", "Settings wasn't saved");
        });
    };
    SpappComponent.prototype.setPopupActionBannedWordsForUserNames = function (fn) {
        this.openPopupBannedWordsForUserNames = fn;
    };
    SpappComponent.prototype.getBannedWordsForUserNamesSettings = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(this.bannedWordsForUserNamePoliceURL)
            .then(function (settings) {
            if (settings.data && settings.data.length > 0) {
                settings.data.forEach(function (setting) {
                    _this.bannedWordForUserNamesSettings._id = setting._id;
                    _this.bannedWordForUserNamesSettings.enabled = setting.enabled;
                    _this.bannedWordForUserNamesSettings.companyName = setting.companyName;
                    _this.bannedWordForUserNamesSettings.words = setting.words;
                });
            }
            else {
                console.log('Nothing retrieved');
            }
        }).catch(function (err) {
            _this._service.error("Error", "Failed to retrieve banned words for user names settings");
            console.log('Nothing retrieved. Error: ' + err);
        });
    };
    SpappComponent.prototype.bannedWordForUserNamesSettingsSaveButonEnabled = function () {
        return this.bannedWordForUserNamesSettings.companyName && this.bannedWordForUserNamesSettings.companyName.trim().length > 0;
    };
    SpappComponent.prototype.saveBannedWordsForUserNamesSettings = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post(this.bannedWordsForUserNamePoliceURL, this.bannedWordForUserNamesSettings)
            .then(function (response) {
            if (response.data) {
                _this.bannedWordForUserNamesSettings._id = response.data._id;
                _this._service.success("Success", "Settings saved!");
            }
        }).catch(function (err) {
            console.log(err);
            _this._service.error("Error", "Settings wasn't saved");
        });
    };
    SpappComponent.prototype.setPopupActionAddCmd = function (fn) {
        this.openPopupAddCmd = fn;
    };
    SpappComponent.prototype.resetNewCommandForm = function () {
        this.newCommand.name = '';
        this.newCommand.details = '';
    };
    SpappComponent.prototype.selectCommand = function (command) {
        this.selectedCommand = command;
    };
    SpappComponent.prototype.getCommands = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(this.commandsURL)
            .then(function (commands) {
            if (commands.data && commands.data.length > 0) {
                commands.data.forEach(function (command) {
                    _this.commands.push(command);
                });
            }
            else {
                console.log('Nothing retrieved');
            }
        }).catch(function (err) {
            _this._service.error("Error", "Failed to retrieve commands");
            console.log('Nothing retrieved. Error: ' + err);
        });
    };
    SpappComponent.prototype.deleteCommand = function (commandId) {
        var _this = this;
        var idx = this.getIndexOfCommand(commandId);
        if (idx !== -1) {
            var commandToBeDeleted = this.commands[idx];
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete(this.commandsURL + "/" + commandToBeDeleted._id).then(function (response) {
                _this.commands.splice(idx, 1);
                _this.selectCommand(null);
                _this._service.success("Success", "Command Deleted!");
            }).catch(function (err) {
                _this._service.error("Error", "Failed to delete command");
                console.log("error: " + err);
            });
        }
    };
    SpappComponent.prototype.addNewCommand = function () {
        var _this = this;
        if (this.newCommand.name.length <= 0 || this.newCommand.details.length <= 0) {
            this._service.error("Error", "Empty contents");
            return;
        }
        this.newCommand.name = this.newCommand.name.toLowerCase();
        if (this.newCommand.name.charAt(0) == '!') {
            this.newCommand.name = this.newCommand.name.substring(1, this.newCommand.name.length);
        }
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post(this.commandsURL, this.newCommand).then(function (response) {
            if (response.data) {
                _this.newCommand._id = response.data._id;
                _this.commands.push(__WEBPACK_IMPORTED_MODULE_0_lodash__["cloneDeep"](_this.newCommand));
                _this.selectCommand(_this.newCommand);
                _this.newCommand = new __WEBPACK_IMPORTED_MODULE_6__classes_command__["a" /* Command */]();
                _this._service.success("Success", "Command Created!");
            }
        }).catch(function (err) {
            console.log(err);
            _this._service.error("Error", "Command wasn't added");
        });
    };
    SpappComponent.prototype.updateCommand = function (command) {
        var idx = this.getIndexOfCommand(command._id);
        if (idx !== -1) {
            this.commands[idx] = command;
            this.selectCommand(command);
        }
        return this.commands;
    };
    SpappComponent.prototype.setPopupActionWelcomeMsg = function (fn) {
        this.openPopupWelcomeMsg = fn;
    };
    SpappComponent.prototype.setPopupActionBotMsgInDM = function (fn) {
        this.openPopupBotMsgInDM = fn;
    };
    SpappComponent.prototype.setPopupActionBanMsg = function (fn) {
        this.openPopupBanMsg = fn;
    };
    SpappComponent.prototype.setPopupActionHelpMsg = function (fn) {
        this.openPopupHelpMsg = fn;
    };
    SpappComponent.prototype.saveButonEnabled = function () {
        return this.settings.botMsgInDM && this.settings.botMsgInDM.length > 0 && this.settings.botMsgInDM.search('{commands}') >= 0;
    };
    SpappComponent.prototype.setAggregatedUserValuesForWelcomeMessage = function (number) {
        this.settings.welcomeAggregatorNumber = number;
    };
    SpappComponent.prototype.composeAggregatorDropdownMsg = function (number) {
        var aggregatorPhrase = "";
        if (number == 0) {
            aggregatorPhrase = "Bot is set to not greet any new user";
        }
        else if (number == 1) {
            aggregatorPhrase = "Bot is set to greet every new user";
        }
        else {
            aggregatorPhrase = "Bot is set to greet " + number + " new users together";
        }
        return aggregatorPhrase;
    };
    SpappComponent.prototype.saveSettings = function () {
        var _this = this;
        if (!this.settings.botMsgInDM || this.settings.botMsgInDM.trim().length <= 0) {
            this._service.error("Error", "Cannot leave 'Bot response in DM' empty");
            return;
        }
        if (this.settings.botMsgInDM.search('{commands}') <= 0) {
            this._service.error("Error", "You have to add {commands} in 'Bot response in DM'");
            return;
        }
        //this.settings.botMsgInDM = _.replace(this.settings.botMsgInDM, "{commands}", "\n\n{commands}");
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post(this.settingsURL, this.settings).then(function () {
            _this._service.success("Success", "Settings have been saved");
            _this.getSettings();
        }).catch(function (err) {
            console.log(err);
            _this.settings.welcomeAggregatorNumber = 0;
            _this._service.error("Error", "Settings wasn't changed");
        });
    };
    SpappComponent.prototype.getSettings = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(this.settingsURL).then(function (setting) {
            if (setting.data.length > 0) {
                _this.settings.welcomeMsg = setting.data[0].welcomeMsg;
                _this.settings.banMsg = setting.data[0].banMsg;
                _this.settings.helpMsg = setting.data[0].helpMsg;
                _this.settings.botMsgInDM = setting.data[0].botMsgInDM;
                // this.settings.botMsgInDM = _.replace(setting.data[0].botMsgInDM, "\n\n", "");
                _this.settings.welcomeAggregatorNumber = setting.data[0].welcomeAggregatorNumber || 0;
            }
            else {
                console.log("No settings found.");
                _this.settings.welcomeAggregatorNumber = 0;
            }
        }).catch(function (err) {
            console.log(err);
            _this._service.error("Error", "Couldn't retrieve bot settings");
        });
    };
    SpappComponent.prototype.setPopupActionNewScheduledMsg = function (fn) {
        this.openPopupNewScheduledMsg = fn;
    };
    SpappComponent.prototype.selectScheduledMsg = function (scheduledMsg) {
        this.selectedScheduledMsg = scheduledMsg;
    };
    SpappComponent.prototype.composeTimerDropdownMsg = function (interval) {
        var timerPhrase = "";
        if (interval > 0) {
            var diff = __WEBPACK_IMPORTED_MODULE_5_moment__["duration"](interval);
            if (diff.asHours() < 1) {
                timerPhrase = "Timer set as once every " + diff.asMinutes() + " minutes";
            }
            else {
                timerPhrase = "Timer set as once every " + diff.asHours() + " hours";
            }
        }
        else {
            timerPhrase = "Timer is turned off";
        }
        return timerPhrase;
    };
    SpappComponent.prototype.setSchedulerTimer = function (interval) {
        this.newScheduledMsg.interval = interval * 60 * 1000;
    };
    SpappComponent.prototype.getScheduledMsgs = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(this.scheduledMsgsURL)
            .then(function (scheduledMsgs) {
            if (scheduledMsgs.data && scheduledMsgs.data.length > 0) {
                scheduledMsgs.data.forEach(function (command) {
                    _this.scheduledMsgs.push(command);
                });
            }
            else {
                console.log('Nothing retrieved');
            }
        }).catch(function (err) {
            _this._service.error("Error", "Failed to retrieve schedule messages");
            console.log('Nothing retrieved. Error: ' + err);
        });
    };
    SpappComponent.prototype.resetNewScheduledMsgForm = function () {
        this.newScheduledMsg.interval = 0;
        this.newScheduledMsg = new __WEBPACK_IMPORTED_MODULE_7__classes_schedule_message__["a" /* ScheduleMessage */]();
        this.addNewMsgTabHeader = "Add new Message";
    };
    SpappComponent.prototype.editScheduledMsg = function (scheduledMsgId) {
        var idx = this.getIndexOfScheduledMsg(scheduledMsgId);
        if (idx !== -1) {
            this.addNewMsgTabHeader = "Edit Message";
            this.scheduledMsgsTabs.tabs[1].active = true;
            this.newScheduledMsg = __WEBPACK_IMPORTED_MODULE_0_lodash__["cloneDeep"](this.scheduledMsgs[idx]);
        }
    };
    SpappComponent.prototype.deleteScheduledMsg = function (scheduledMsgId) {
        var _this = this;
        var idx = this.getIndexOfScheduledMsg(scheduledMsgId);
        if (idx !== -1) {
            var scheduledMsgToBeDeleted = this.scheduledMsgs[idx];
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.delete(this.scheduledMsgsURL + "/" + scheduledMsgToBeDeleted._id).then(function (response) {
                _this.scheduledMsgs.splice(idx, 1);
                _this.selectScheduledMsg(null);
                _this._service.success("Success", "Message Deleted!");
            }).catch(function (err) {
                _this._service.error("Error", "Failed to delete command");
                console.log("error: " + err);
            });
        }
    };
    SpappComponent.prototype.saveScheduledMsgButonEnabled = function () {
        return this.newScheduledMsg.text && this.newScheduledMsg.text.length > 0;
    };
    SpappComponent.prototype.addNewScheduledMsg = function () {
        var _this = this;
        if (this.newScheduledMsg.text.length <= 0) {
            this._service.error("Error", "Empty message cannot be saved");
            return;
        }
        var oldRecId = this.newScheduledMsg._id;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post(this.scheduledMsgsURL, this.newScheduledMsg).then(function (response) {
            if (response.data) {
                if (oldRecId) {
                    var idx = _this.getIndexOfScheduledMsg(oldRecId);
                    if (idx !== -1) {
                        _this.scheduledMsgs.splice(idx, 1);
                    }
                }
                _this.addNewMsgTabHeader = "Add new Message";
                _this.newScheduledMsg._id = response.data._id;
                _this.scheduledMsgs.push(__WEBPACK_IMPORTED_MODULE_0_lodash__["cloneDeep"](_this.newScheduledMsg));
                _this.selectScheduledMsg(_this.newScheduledMsg);
                _this.newScheduledMsg = new __WEBPACK_IMPORTED_MODULE_7__classes_schedule_message__["a" /* ScheduleMessage */]();
                _this._service.success("Success", "Message Saved!");
                _this.scheduledMsgsTabs.tabs[0].active = true;
            }
        }).catch(function (err) {
            console.log(err);
            _this._service.error("Error", "Message wasn't saved");
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewChild"])('scheduledMsgsTabs'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ngx_bootstrap__["d" /* TabsetComponent */])
    ], SpappComponent.prototype, "scheduledMsgsTabs", void 0);
    SpappComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Component"])({
            selector: 'spapp',
            template: __webpack_require__("../../../../../src/app/components/spapp/spapp.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/spapp/spapp.component.css")],
            encapsulation: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ViewEncapsulation"].None
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angular2_notifications__["NotificationsService"],
            __WEBPACK_IMPORTED_MODULE_11__services_LoginController__["a" /* LoginService */]])
    ], SpappComponent);
    return SpappComponent;
}());



/***/ }),

/***/ "../../../../../src/app/services/LoginController.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_user__ = __webpack_require__("../../../../../src/app/classes/user.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__("../../../../axios/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginService = (function () {
    function LoginService(_router) {
        this._router = _router;
        this.user = new __WEBPACK_IMPORTED_MODULE_1__classes_user__["a" /* User */]();
        // private serverURL = 'https://tbot-demo.herokuapp.com';
        // private authURL = this.serverURL.concat('/auth');
        this.authURL = '/auth';
    }
    LoginService.prototype.isUserLoggedIn = function () {
        return this.user.isUserLoggedIn;
    };
    LoginService.prototype.isUserAuthorized = function () {
        if (!this.user.isUserLoggedIn) {
            this._router.navigate([""]);
        }
    };
    LoginService.prototype.currentLoggedInUser = function () {
        return this.user;
    };
    LoginService.prototype.logout = function () {
        this.user.isUserLoggedIn = false;
        this._router.navigate([""]);
    };
    LoginService.prototype.changePWD = function (pwd) {
        var _this = this;
        var userObj = new __WEBPACK_IMPORTED_MODULE_1__classes_user__["a" /* User */]();
        userObj._id = this.user._id;
        userObj.username = this.user.username;
        userObj.password = pwd;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post("" + _this.authURL, userObj)
                .then(function (response) {
                _this.user._id = response.data._id;
                _this.user.password = userObj.password;
                resolve(true);
            }).catch(function () {
                return reject(false);
            });
        });
    };
    LoginService.prototype.doLogin = function (loginData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get(_this.authURL + "/" + loginData.username)
                .then(function (response) {
                if (response.data) {
                    if (response.data.password == loginData.password) {
                        _this.user._id = response.data._id;
                        _this.user.username = loginData.username;
                        _this.user.password = loginData.password;
                        _this.user.isUserLoggedIn = true;
                        resolve(true);
                    }
                    else {
                        return resolve(false);
                    }
                }
                else {
                    return resolve(false);
                }
            }).catch(function () {
                return reject(false);
            });
        });
    };
    LoginService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__("../../../../../src/polyfills.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");





if (__WEBPACK_IMPORTED_MODULE_4__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_2__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_3__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../../src/polyfills.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es7_reflect__ = __webpack_require__("../../../../core-js/es7/reflect.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_zone_js_dist_zone__ = __webpack_require__("../../../../zone.js/dist/zone.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_zone_js_dist_zone__);
/**
 * This file includes polyfills needed by Angular and is loaded before the app.
 * You can add your own extra polyfills to this file.
 *
 * This file is divided into 2 sections:
 *   1. Browser polyfills. These are applied before loading ZoneJS and are sorted by browsers.
 *   2. Application imports. Files imported after ZoneJS that should be loaded before your main
 *      file.
 *
 * The current setup is for so-called "evergreen" browsers; the last versions of browsers that
 * automatically update themselves. This includes Safari >= 10, Chrome >= 55 (including Opera),
 * Edge >= 13 on the desktop, and iOS 10 and Chrome on mobile.
 *
 * Learn more in https://angular.io/docs/ts/latest/guide/browser-support.html
 */
/***************************************************************************************************
 * BROWSER POLYFILLS
 */
/** IE9, IE10 and IE11 requires all of the following polyfills. **/
// import 'core-js/es6/symbol';
// import 'core-js/es6/object';
// import 'core-js/es6/function';
// import 'core-js/es6/parse-int';
// import 'core-js/es6/parse-float';
// import 'core-js/es6/number';
// import 'core-js/es6/math';
// import 'core-js/es6/string';
// import 'core-js/es6/date';
// import 'core-js/es6/array';
// import 'core-js/es6/regexp';
// import 'core-js/es6/map';
// import 'core-js/es6/weak-map';
// import 'core-js/es6/set';
/** IE10 and IE11 requires the following for NgClass support on SVG elements */
// import 'classlist.js';  // Run `npm install --save classlist.js`.
/** IE10 and IE11 requires the following for the Reflect API. */
// import 'core-js/es6/reflect';
/** Evergreen browsers require these. **/
// Used for reflect-metadata in JIT. If you use AOT (and only Angular decorators), you can remove.

/**
 * Required to support Web Animations `@angular/platform-browser/animations`.
 * Needed for: All but Chrome, Firefox and Opera. http://caniuse.com/#feat=web-animation
 **/
// import 'web-animations-js';  // Run `npm install --save web-animations-js`.
/***************************************************************************************************
 * Zone JS is required by default for Angular itself.
 */
 // Included with Angular CLI.
/***************************************************************************************************
 * APPLICATION IMPORTS
 */


/***/ }),

/***/ "../../../../moment/locale recursive ^\\.\\/.*$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../../../../moment/locale/af.js",
	"./af.js": "../../../../moment/locale/af.js",
	"./ar": "../../../../moment/locale/ar.js",
	"./ar-dz": "../../../../moment/locale/ar-dz.js",
	"./ar-dz.js": "../../../../moment/locale/ar-dz.js",
	"./ar-kw": "../../../../moment/locale/ar-kw.js",
	"./ar-kw.js": "../../../../moment/locale/ar-kw.js",
	"./ar-ly": "../../../../moment/locale/ar-ly.js",
	"./ar-ly.js": "../../../../moment/locale/ar-ly.js",
	"./ar-ma": "../../../../moment/locale/ar-ma.js",
	"./ar-ma.js": "../../../../moment/locale/ar-ma.js",
	"./ar-sa": "../../../../moment/locale/ar-sa.js",
	"./ar-sa.js": "../../../../moment/locale/ar-sa.js",
	"./ar-tn": "../../../../moment/locale/ar-tn.js",
	"./ar-tn.js": "../../../../moment/locale/ar-tn.js",
	"./ar.js": "../../../../moment/locale/ar.js",
	"./az": "../../../../moment/locale/az.js",
	"./az.js": "../../../../moment/locale/az.js",
	"./be": "../../../../moment/locale/be.js",
	"./be.js": "../../../../moment/locale/be.js",
	"./bg": "../../../../moment/locale/bg.js",
	"./bg.js": "../../../../moment/locale/bg.js",
	"./bm": "../../../../moment/locale/bm.js",
	"./bm.js": "../../../../moment/locale/bm.js",
	"./bn": "../../../../moment/locale/bn.js",
	"./bn.js": "../../../../moment/locale/bn.js",
	"./bo": "../../../../moment/locale/bo.js",
	"./bo.js": "../../../../moment/locale/bo.js",
	"./br": "../../../../moment/locale/br.js",
	"./br.js": "../../../../moment/locale/br.js",
	"./bs": "../../../../moment/locale/bs.js",
	"./bs.js": "../../../../moment/locale/bs.js",
	"./ca": "../../../../moment/locale/ca.js",
	"./ca.js": "../../../../moment/locale/ca.js",
	"./cs": "../../../../moment/locale/cs.js",
	"./cs.js": "../../../../moment/locale/cs.js",
	"./cv": "../../../../moment/locale/cv.js",
	"./cv.js": "../../../../moment/locale/cv.js",
	"./cy": "../../../../moment/locale/cy.js",
	"./cy.js": "../../../../moment/locale/cy.js",
	"./da": "../../../../moment/locale/da.js",
	"./da.js": "../../../../moment/locale/da.js",
	"./de": "../../../../moment/locale/de.js",
	"./de-at": "../../../../moment/locale/de-at.js",
	"./de-at.js": "../../../../moment/locale/de-at.js",
	"./de-ch": "../../../../moment/locale/de-ch.js",
	"./de-ch.js": "../../../../moment/locale/de-ch.js",
	"./de.js": "../../../../moment/locale/de.js",
	"./dv": "../../../../moment/locale/dv.js",
	"./dv.js": "../../../../moment/locale/dv.js",
	"./el": "../../../../moment/locale/el.js",
	"./el.js": "../../../../moment/locale/el.js",
	"./en-au": "../../../../moment/locale/en-au.js",
	"./en-au.js": "../../../../moment/locale/en-au.js",
	"./en-ca": "../../../../moment/locale/en-ca.js",
	"./en-ca.js": "../../../../moment/locale/en-ca.js",
	"./en-gb": "../../../../moment/locale/en-gb.js",
	"./en-gb.js": "../../../../moment/locale/en-gb.js",
	"./en-ie": "../../../../moment/locale/en-ie.js",
	"./en-ie.js": "../../../../moment/locale/en-ie.js",
	"./en-nz": "../../../../moment/locale/en-nz.js",
	"./en-nz.js": "../../../../moment/locale/en-nz.js",
	"./eo": "../../../../moment/locale/eo.js",
	"./eo.js": "../../../../moment/locale/eo.js",
	"./es": "../../../../moment/locale/es.js",
	"./es-do": "../../../../moment/locale/es-do.js",
	"./es-do.js": "../../../../moment/locale/es-do.js",
	"./es-us": "../../../../moment/locale/es-us.js",
	"./es-us.js": "../../../../moment/locale/es-us.js",
	"./es.js": "../../../../moment/locale/es.js",
	"./et": "../../../../moment/locale/et.js",
	"./et.js": "../../../../moment/locale/et.js",
	"./eu": "../../../../moment/locale/eu.js",
	"./eu.js": "../../../../moment/locale/eu.js",
	"./fa": "../../../../moment/locale/fa.js",
	"./fa.js": "../../../../moment/locale/fa.js",
	"./fi": "../../../../moment/locale/fi.js",
	"./fi.js": "../../../../moment/locale/fi.js",
	"./fo": "../../../../moment/locale/fo.js",
	"./fo.js": "../../../../moment/locale/fo.js",
	"./fr": "../../../../moment/locale/fr.js",
	"./fr-ca": "../../../../moment/locale/fr-ca.js",
	"./fr-ca.js": "../../../../moment/locale/fr-ca.js",
	"./fr-ch": "../../../../moment/locale/fr-ch.js",
	"./fr-ch.js": "../../../../moment/locale/fr-ch.js",
	"./fr.js": "../../../../moment/locale/fr.js",
	"./fy": "../../../../moment/locale/fy.js",
	"./fy.js": "../../../../moment/locale/fy.js",
	"./gd": "../../../../moment/locale/gd.js",
	"./gd.js": "../../../../moment/locale/gd.js",
	"./gl": "../../../../moment/locale/gl.js",
	"./gl.js": "../../../../moment/locale/gl.js",
	"./gom-latn": "../../../../moment/locale/gom-latn.js",
	"./gom-latn.js": "../../../../moment/locale/gom-latn.js",
	"./gu": "../../../../moment/locale/gu.js",
	"./gu.js": "../../../../moment/locale/gu.js",
	"./he": "../../../../moment/locale/he.js",
	"./he.js": "../../../../moment/locale/he.js",
	"./hi": "../../../../moment/locale/hi.js",
	"./hi.js": "../../../../moment/locale/hi.js",
	"./hr": "../../../../moment/locale/hr.js",
	"./hr.js": "../../../../moment/locale/hr.js",
	"./hu": "../../../../moment/locale/hu.js",
	"./hu.js": "../../../../moment/locale/hu.js",
	"./hy-am": "../../../../moment/locale/hy-am.js",
	"./hy-am.js": "../../../../moment/locale/hy-am.js",
	"./id": "../../../../moment/locale/id.js",
	"./id.js": "../../../../moment/locale/id.js",
	"./is": "../../../../moment/locale/is.js",
	"./is.js": "../../../../moment/locale/is.js",
	"./it": "../../../../moment/locale/it.js",
	"./it.js": "../../../../moment/locale/it.js",
	"./ja": "../../../../moment/locale/ja.js",
	"./ja.js": "../../../../moment/locale/ja.js",
	"./jv": "../../../../moment/locale/jv.js",
	"./jv.js": "../../../../moment/locale/jv.js",
	"./ka": "../../../../moment/locale/ka.js",
	"./ka.js": "../../../../moment/locale/ka.js",
	"./kk": "../../../../moment/locale/kk.js",
	"./kk.js": "../../../../moment/locale/kk.js",
	"./km": "../../../../moment/locale/km.js",
	"./km.js": "../../../../moment/locale/km.js",
	"./kn": "../../../../moment/locale/kn.js",
	"./kn.js": "../../../../moment/locale/kn.js",
	"./ko": "../../../../moment/locale/ko.js",
	"./ko.js": "../../../../moment/locale/ko.js",
	"./ky": "../../../../moment/locale/ky.js",
	"./ky.js": "../../../../moment/locale/ky.js",
	"./lb": "../../../../moment/locale/lb.js",
	"./lb.js": "../../../../moment/locale/lb.js",
	"./lo": "../../../../moment/locale/lo.js",
	"./lo.js": "../../../../moment/locale/lo.js",
	"./lt": "../../../../moment/locale/lt.js",
	"./lt.js": "../../../../moment/locale/lt.js",
	"./lv": "../../../../moment/locale/lv.js",
	"./lv.js": "../../../../moment/locale/lv.js",
	"./me": "../../../../moment/locale/me.js",
	"./me.js": "../../../../moment/locale/me.js",
	"./mi": "../../../../moment/locale/mi.js",
	"./mi.js": "../../../../moment/locale/mi.js",
	"./mk": "../../../../moment/locale/mk.js",
	"./mk.js": "../../../../moment/locale/mk.js",
	"./ml": "../../../../moment/locale/ml.js",
	"./ml.js": "../../../../moment/locale/ml.js",
	"./mr": "../../../../moment/locale/mr.js",
	"./mr.js": "../../../../moment/locale/mr.js",
	"./ms": "../../../../moment/locale/ms.js",
	"./ms-my": "../../../../moment/locale/ms-my.js",
	"./ms-my.js": "../../../../moment/locale/ms-my.js",
	"./ms.js": "../../../../moment/locale/ms.js",
	"./mt": "../../../../moment/locale/mt.js",
	"./mt.js": "../../../../moment/locale/mt.js",
	"./my": "../../../../moment/locale/my.js",
	"./my.js": "../../../../moment/locale/my.js",
	"./nb": "../../../../moment/locale/nb.js",
	"./nb.js": "../../../../moment/locale/nb.js",
	"./ne": "../../../../moment/locale/ne.js",
	"./ne.js": "../../../../moment/locale/ne.js",
	"./nl": "../../../../moment/locale/nl.js",
	"./nl-be": "../../../../moment/locale/nl-be.js",
	"./nl-be.js": "../../../../moment/locale/nl-be.js",
	"./nl.js": "../../../../moment/locale/nl.js",
	"./nn": "../../../../moment/locale/nn.js",
	"./nn.js": "../../../../moment/locale/nn.js",
	"./pa-in": "../../../../moment/locale/pa-in.js",
	"./pa-in.js": "../../../../moment/locale/pa-in.js",
	"./pl": "../../../../moment/locale/pl.js",
	"./pl.js": "../../../../moment/locale/pl.js",
	"./pt": "../../../../moment/locale/pt.js",
	"./pt-br": "../../../../moment/locale/pt-br.js",
	"./pt-br.js": "../../../../moment/locale/pt-br.js",
	"./pt.js": "../../../../moment/locale/pt.js",
	"./ro": "../../../../moment/locale/ro.js",
	"./ro.js": "../../../../moment/locale/ro.js",
	"./ru": "../../../../moment/locale/ru.js",
	"./ru.js": "../../../../moment/locale/ru.js",
	"./sd": "../../../../moment/locale/sd.js",
	"./sd.js": "../../../../moment/locale/sd.js",
	"./se": "../../../../moment/locale/se.js",
	"./se.js": "../../../../moment/locale/se.js",
	"./si": "../../../../moment/locale/si.js",
	"./si.js": "../../../../moment/locale/si.js",
	"./sk": "../../../../moment/locale/sk.js",
	"./sk.js": "../../../../moment/locale/sk.js",
	"./sl": "../../../../moment/locale/sl.js",
	"./sl.js": "../../../../moment/locale/sl.js",
	"./sq": "../../../../moment/locale/sq.js",
	"./sq.js": "../../../../moment/locale/sq.js",
	"./sr": "../../../../moment/locale/sr.js",
	"./sr-cyrl": "../../../../moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../../../../moment/locale/sr-cyrl.js",
	"./sr.js": "../../../../moment/locale/sr.js",
	"./ss": "../../../../moment/locale/ss.js",
	"./ss.js": "../../../../moment/locale/ss.js",
	"./sv": "../../../../moment/locale/sv.js",
	"./sv.js": "../../../../moment/locale/sv.js",
	"./sw": "../../../../moment/locale/sw.js",
	"./sw.js": "../../../../moment/locale/sw.js",
	"./ta": "../../../../moment/locale/ta.js",
	"./ta.js": "../../../../moment/locale/ta.js",
	"./te": "../../../../moment/locale/te.js",
	"./te.js": "../../../../moment/locale/te.js",
	"./tet": "../../../../moment/locale/tet.js",
	"./tet.js": "../../../../moment/locale/tet.js",
	"./th": "../../../../moment/locale/th.js",
	"./th.js": "../../../../moment/locale/th.js",
	"./tl-ph": "../../../../moment/locale/tl-ph.js",
	"./tl-ph.js": "../../../../moment/locale/tl-ph.js",
	"./tlh": "../../../../moment/locale/tlh.js",
	"./tlh.js": "../../../../moment/locale/tlh.js",
	"./tr": "../../../../moment/locale/tr.js",
	"./tr.js": "../../../../moment/locale/tr.js",
	"./tzl": "../../../../moment/locale/tzl.js",
	"./tzl.js": "../../../../moment/locale/tzl.js",
	"./tzm": "../../../../moment/locale/tzm.js",
	"./tzm-latn": "../../../../moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../../../../moment/locale/tzm-latn.js",
	"./tzm.js": "../../../../moment/locale/tzm.js",
	"./uk": "../../../../moment/locale/uk.js",
	"./uk.js": "../../../../moment/locale/uk.js",
	"./ur": "../../../../moment/locale/ur.js",
	"./ur.js": "../../../../moment/locale/ur.js",
	"./uz": "../../../../moment/locale/uz.js",
	"./uz-latn": "../../../../moment/locale/uz-latn.js",
	"./uz-latn.js": "../../../../moment/locale/uz-latn.js",
	"./uz.js": "../../../../moment/locale/uz.js",
	"./vi": "../../../../moment/locale/vi.js",
	"./vi.js": "../../../../moment/locale/vi.js",
	"./x-pseudo": "../../../../moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../../../../moment/locale/x-pseudo.js",
	"./yo": "../../../../moment/locale/yo.js",
	"./yo.js": "../../../../moment/locale/yo.js",
	"./zh-cn": "../../../../moment/locale/zh-cn.js",
	"./zh-cn.js": "../../../../moment/locale/zh-cn.js",
	"./zh-hk": "../../../../moment/locale/zh-hk.js",
	"./zh-hk.js": "../../../../moment/locale/zh-hk.js",
	"./zh-tw": "../../../../moment/locale/zh-tw.js",
	"./zh-tw.js": "../../../../moment/locale/zh-tw.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../moment/locale recursive ^\\.\\/.*$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map