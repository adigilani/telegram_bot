var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

// BOT Settings
const BOT_SETTINGS = require('./telegram-bot-settings.json');

var USER_COLLECTION = "users";
var COMMANDS_COLLECTION = "commands";
var SCHEDULE_MSGS_COLLECTION = "scheduleMsgs";
var SETTINGS_COLLECTION = "settings";
var BANNED_WORDS_COLLECTION = "bannedWords";
var FRIENDLY_GROUP_COLLECTION = "friendlyGroups";
var AUTH_COLLECTION = "admin";
const MONGODB_URI = process.env.MONGODB_URI || BOT_SETTINGS.MONGODB_URI;

var app = express();
app.use(bodyParser.json());

// Create link to Angular build directory
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;

var botManager;

// Connect to the database before starting the application server.
mongodb.MongoClient.connect(MONGODB_URI, function (err, database) {
  if (err) {
    console.log(err);
    process.exit(1);
  }

  // Save database object from the callback for reuse.
  db = database;
  console.log("Database connection ready");

  // bootstrap settings
  botManager.retrieveBannedWordsDictionary();
  botManager.retrieveFriendlyGroupsSettings();
  botManager.retrieveScheduledMsgs();
  botManager.retrieveBotSettings();

  // Initialize the app.
  var server = app.listen(process.env.PORT || 3000, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});


// API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({"error": message});
}

////////////////////////////////////////////// BOT SETTINGS ////////////////////////////////////////////////////////////////////

/*  "/settings"
  *  GET: get settings
*/
app.get("/settings", function(req, res) {
  db.collection(SETTINGS_COLLECTION).find().toArray(function(err, docs) {
    if (err) {
      handleError(res, err.message, "Failed to get settings.");
    } else {
      res.status(200).json(docs);
    }
  });
});

/*  "/settings"
  *  POST: create or update bot settings
*/
app.post("/settings", function(req, res) {
  var settings = req.body;
  botManager.updateBotSettings(settings);
  db.collection(SETTINGS_COLLECTION).find().toArray(function(err, docs) {
    if (err) {
      db.collection(SETTINGS_COLLECTION).insertOne(settings, function(err, doc) {
        if (err) {
          handleError(res, err.message, "Failed to create new settings");
        } else {
          res.status(201).json(doc.ops[0]);
        }
      });
    } else {
      if(docs && docs.length>0) {
        const id = docs[0]._id
        delete settings._id;
        db.collection(SETTINGS_COLLECTION).updateOne({_id: new ObjectID(id)}, settings, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to update settings");
          } else {
            settings._id = id;
            res.status(200).json(settings);
          }
        });
      } else {
        db.collection(SETTINGS_COLLECTION).insertOne(settings, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to create new setings msg.");
          } else {
            res.status(201).json(doc.ops[0]);
          }
        });
      }
    }
  });
});

////////////////////////////////////////////// authentication ////////////////////////////////////////////////////////////////////

/*  "/auth"
  *  GET: get all scheduled msgs
*/
app.get("/auth/:username", function(req, res) {
  db.collection(AUTH_COLLECTION).findOne({username: req.params.username}, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to find user.");
      } else {
        res.status(200).json(doc);
      }
    });
});

app.post("/auth", function(req, res) {
  var credentials = req.body;
  const id = credentials._id;
  delete credentials._id;
  delete credentials.isUserLoggedIn;

  db.collection(AUTH_COLLECTION).updateOne({_id: new ObjectID(id)}, credentials, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update user credentials");
    } else {
      credentials._id = id;
      res.status(200).json(credentials);
    }
  });
});



////////////////////////////////////////////// SCHEDULE MSGS ////////////////////////////////////////////////////////////////////

/*  "/scheduled"
  *  GET: get all scheduled msgs
*/
app.get("/scheduled", function(req, res) {
  db.collection(SCHEDULE_MSGS_COLLECTION).find().toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get scheduled msgs.");
      } else {
        res.status(200).json(docs);
      }
    });
});

/*  "/scheduled"
  *  POST: create new message
*/
app.post("/scheduled", function(req, res) {
  var newScheduledMsg = req.body;

  if(newScheduledMsg._id) {
    const recId = newScheduledMsg._id
    delete newScheduledMsg._id;

    console.log("Updating a previous scheduled message");

    db.collection(SCHEDULE_MSGS_COLLECTION).updateOne({_id: new ObjectID(recId)}, newScheduledMsg, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to update scheduled msg");
      } else {
        newScheduledMsg._id = recId;
        botManager.addNewMsgToScheduler(newScheduledMsg);
        res.status(200).json(newScheduledMsg);
      }
    });
  } else {
    console.log("Adding a new scheduled message");

    db.collection(SCHEDULE_MSGS_COLLECTION).insertOne(newScheduledMsg, function(err, doc) {
      if (err) {
        handleError(res, err.message, "Failed to create new scheduled msg.");
      } else {
        botManager.addNewMsgToScheduler(newScheduledMsg);
        res.status(201).json(doc.ops[0]);
      }
    });
  }
});

/*   "/scheduled/:id"
  *   Delete: delete message by doc id
*/
app.delete("/scheduled/:id", function(req, res) {
  db.collection(SCHEDULE_MSGS_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete scheduled msg");
    } else {
      botManager.deleteMsgFromScheduler(req.params.id);
      res.status(200).json(req.params.id);
    }
  });
});


////////////////////////////////////////////// FRIENDLY GROUPS  ////////////////////////////////////////////////////////////////////

/*  "/police/friendlygroups"
  *  GET: get all friendly groups
*/
app.get("/police/friendlygroups", function(req, res) {
  db.collection(FRIENDLY_GROUP_COLLECTION).find().toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get friendly groups dictionary.");
      } else {
        res.status(200).json(docs);
      }
    });
});

/*  "/police/friendlygroups"
  *  POST: create new friendly group settings
*/
app.post("/police/friendlygroups", function(req, res) {
  var newFriendlyGroup = req.body;

  db.collection(FRIENDLY_GROUP_COLLECTION).find().toArray(function(err, docs) {
    if (err) {
      db.collection(FRIENDLY_GROUP_COLLECTION).insertOne(newFriendlyGroup, function(err, doc) {
        if (err) {
          handleError(res, err.message, "Failed to add friendly group record.");
        } else {
          botManager.updateFriendlyGroupSettings(newFriendlyGroup);
          res.status(201).json(doc.ops[0]);
        }
      });
    } else {
      if(docs && docs.length>0) {
        const id = docs[0]._id
        delete newFriendlyGroup._id;
        db.collection(FRIENDLY_GROUP_COLLECTION).updateOne({_id: new ObjectID(id)}, newFriendlyGroup, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to update friendly group record.");
          } else {
            botManager.updateFriendlyGroupSettings(newFriendlyGroup);
            newFriendlyGroup._id = id;
            res.status(200).json(newFriendlyGroup);
          }
        });
      } else {
        db.collection(FRIENDLY_GROUP_COLLECTION).insertOne(newFriendlyGroup, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to add friendly group record.");
          } else {
            botManager.updateFriendlyGroupSettings(newFriendlyGroup);
            res.status(201).json(doc.ops[0]);
          }
        });
      }
    }
  });
});


////////////////////////////////////////////// BANNED WORDS  ////////////////////////////////////////////////////////////////////

/*  "/police/bannedwords"
  *  GET: get all banned words
*/
app.get("/police/bannedwords", function(req, res) {
  db.collection(BANNED_WORDS_COLLECTION).find().toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get banned words dictionary.");
      } else {
        res.status(200).json(docs);
      }
    });
});

/*  "/police/bannedwords"
  *  POST: create new banned words dictionary
*/
app.post("/police/bannedwords", function(req, res) {
  var newBannedWordsItem = req.body;

  db.collection(BANNED_WORDS_COLLECTION).find().toArray(function(err, docs) {
    if (err) {
      db.collection(BANNED_WORDS_COLLECTION).insertOne(newBannedWordsItem, function(err, doc) {
        if (err) {
          handleError(res, err.message, "Failed to add friendly group record.");
        } else {
          botManager.updateBannedWordsDictionary(newBannedWordsItem);
          res.status(201).json(doc.ops[0]);
        }
      });
    } else {
      if(docs && docs.length>0) {
        const id = docs[0]._id
        delete newBannedWordsItem._id;
        db.collection(BANNED_WORDS_COLLECTION).updateOne({_id: new ObjectID(id)}, newBannedWordsItem, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to update friendly group record.");
          } else {
            botManager.updateBannedWordsDictionary(newBannedWordsItem);
            newBannedWordsItem._id = id;
            res.status(200).json(newBannedWordsItem);
          }
        });
      } else {
        db.collection(BANNED_WORDS_COLLECTION).insertOne(newBannedWordsItem, function(err, doc) {
          if (err) {
            handleError(res, err.message, "Failed to add friendly group record.");
          } else {
            botManager.updateBannedWordsDictionary(newBannedWordsItem);
            res.status(201).json(doc.ops[0]);
          }
        });
      }
    }
  });
});


////////////////////////////////////////////// COMMANDS ////////////////////////////////////////////////////////////////////

/*  "/commands"
  *  GET: get all commands
*/
app.get("/commands", function(req, res) {
  db.collection(COMMANDS_COLLECTION).find().toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get commands.");
      } else {
        res.status(200).json(docs);
      }
    });
});

/*  "/commands/:commandName"
  *  GET: get specific commands
*/
app.get("/commands/:commandName", function(req, res) {
  db.collection(COMMANDS_COLLECTION).findOne({name: req.params.commandName}, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get user");
    } else {
      res.status(200).json(doc);
    }
  });
});

/*  "/commands"
  *  POST: create new commandng se
*/
app.post("/commands", function(req, res) {
  var newCommand = req.body;

  db.collection(COMMANDS_COLLECTION).insertOne(newCommand, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new command record.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

/*   "/commands/:id"
  *   Delete: delete command by doc id
*/
app.delete("/commands/:id", function(req, res) {
  db.collection(COMMANDS_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete command");
    } else {
      res.status(200).json(req.params.id);
    }
  });
});


////////////////////////////////////////////// USERS ////////////////////////////////////////////////////////////////////

/*   "/users"
  *   POST: creates a new users
*/
app.post("/users", function(req, res) {
  var newUser = req.body;

  if (!req.body.username) {
    handleError(res, "Invalid user input", "Must provide a name.", 400);
  }

  db.collection(USER_COLLECTION).insertOne(newUser, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to create new user record.");
    } else {
      res.status(201).json(doc.ops[0]);
    }
  });
});

/*  "/users"
  *    GET: get all users
*/
app.get("/users", function(req, res) {
  db.collection(USER_COLLECTION).find().toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get users.");
      } else {
        res.status(200).json(docs);
      }
    });
});

/*   "/users/doc/:docid"
  *   GET: find user by record id
*/
app.get("/users/doc/:docid", function(req, res) {
  db.collection(USER_COLLECTION).findOne({ _id: new ObjectID(req.params.id) }, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get user");
    } else {
      res.status(200).json(doc);
    }
  });
});

/*  "/users/username/:username"
  *    GET: find user by username
  */
app.get("/users/username/:username", function(req, res) {
  db.collection(USER_COLLECTION).findOne({username: req.params.userrname}, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to get user");
    } else {
      res.status(200).json(doc);
    }
  });
});

/*   "/users/doc/:docid"
  *   Put: Update user by doc id
*/
app.put("/users/doc/:id", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;

  db.collection(USER_COLLECTION).updateOne({_id: new ObjectID(req.params.id)}, updateDoc, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update user");
    } else {
      updateDoc._id = req.params.id;
      res.status(200).json(updateDoc);
    }
  });
});

/*   "/users/user/:username"
  *   Put: Update user by username
*/
app.put("/users/user/:username", function(req, res) {
  var updateDoc = req.body;
  delete updateDoc._id;

  db.collection(USER_COLLECTION).updateOne({username: req.params.userrname}, updateDoc, function(err, doc) {
    if (err) {
      handleError(res, err.message, "Failed to update user");
    } else {
      updateDoc._id = req.params.id;
      res.status(200).json(updateDoc);
    }
  });
});

/*   "/users/doc/:docid"
  *   Delete: delete user by doc id
*/
app.delete("/users/doc/:docid", function(req, res) {
  db.collection(USER_COLLECTION).deleteOne({_id: new ObjectID(req.params.id)}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete user");
    } else {
      res.status(200).json(req.params.id);
    }
  });
});

/*   "/users/user/:username"
  *   Delete: delete user by username
*/
app.delete("/users/user/:username", function(req, res) {
  db.collection(USER_COLLECTION).deleteOne({username: req.params.username}, function(err, result) {
    if (err) {
      handleError(res, err.message, "Failed to delete user");
    } else {
      res.status(200).json(req.params.id);
    }
  });
});


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = function (botApp) {
  botManager = botApp;
  
  app.post('/' + botApp.bot.token, function (req, res) {
    botManager.bot.processUpdate(req.body);
    res.sendStatus(200);
  });
};